Build instructions:

	git clone https://bitbucket.org/vitalyster/jabjab.git
	git clone https://github.com/JakeWharton/ActionBarSherlock.git

Build from command line: 

	cd ActionBarSherlock/library
	android update lib-project -p .
	cd ../jabjab
	ant debug

Build from IDEA:

	Open project Jabjab
	Project Structure -> Modules -> Import module -> From existing sources... -> (select ActionBarSherlock/library)
	Update all module references to created ActionBarSherlock library
	Rebuild project

Build from Eclipse:

	File -> Import -> Android - Existing Android Code Into Workspace -> (select ActionBarSherlock/library)
	On added project select Android Tools -> Add Support Library...