#!/usr/bin/env python

import re

srjava = open("src/org/bombusmod/locale/SR.java").readlines()

strg = "  public static String MS_OFFLINE = loadString ( \"oollo\" );"

pattern = re.compile("(\s+)?public(\s+)?static(\s+)?String(\s+)?(\w+)(\s+)?=(\s+)?loadString(\s+)?\\((\s+)?\\\"([^\"]+)")

for line in srjava:
	matcher = pattern.match(line)
	if matcher:
		print "<string name=\"%s\">%s</string>" % (matcher.group(5).lower(), matcher.group(10))

#print pattern.match(strg)	