#!/usr/bin/env python

from PIL import Image 

import sys
 

if __name__ == "__main__":	
    im = Image.open(sys.argv[1]) 
    imgwidth, imgheight = im.size 
    width = imgwidth / 8
    height = width
    k = 0
    for i in range(0, imgheight, height): 
        for j in range(0, imgwidth, width): 
            box = (j, i, j + width, i + height) 
            a = im.crop(box) 
	    a.save("png/IMG-%s.png" % k) 
            k +=1 
	
