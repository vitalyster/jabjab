#!/usr/bin/env python

import os
import os.path
import codecs
from sys import argv
from sys import exit

import xml.etree.ElementTree as ET

def get_lang(strings_dict):
	return strings_dict["xmlLang"].strip()

def lang_lookup(strings_dict, en):
	for item in strings_dict.items():
		if item[1].strip() == en.strip():
			return item[0]

def make_dict(strings):
	return dict(item.split("\t") for item in strings)
	
if __name__ == "__main__":	

	strings = filter(lambda line: (not line.startswith("//")) and (len(line.strip()) > 0), map(lambda x: x.strip(), codecs.open(argv[1], "r", "utf-8").readlines()))

	strings_dict = make_dict(strings)

	lang = get_lang(strings_dict)

	if lang == None: 
		print "Not a lang file"
		exit(1)

	langdir = os.path.join("res", "values-"+lang)
	if not os.path.exists(langdir):
		os.makedirs(langdir)

	stringslangxml = os.path.join(langdir, "strings.xml")

	lang_strings = ET.Element("resources")

	eng_strings = ET.parse("res/values/strings.xml")

	for elem in list(eng_strings.getroot()):
		next_string = ET.SubElement(lang_strings, "string", name=elem.attrib.get("name"))
		next_string.text = lang_lookup(strings_dict, elem.text)
	ET.ElementTree(lang_strings).write(stringslangxml)


