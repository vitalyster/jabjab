/*
Copyright (c) 2000,2001 Al Sutton (al@alsutton.com)
All rights reserved.
Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions
and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of
conditions and the following disclaimer in the documentation and/or other materials provided with
the distribution.

Neither the name of Al Sutton nor the names of its contributors may be used to endorse or promote
products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.alsutton.jabber;

import android.net.SSLCertificateSocketFactory;
import com.alsutton.jabber.datablocks.Iq;
import com.alsutton.jabber.datablocks.Message;
import com.alsutton.jabber.datablocks.Presence;
import org.bombusmod.io.DnsSrvResolver;
import org.bombusmod.xml.EndOfStreamException;
import org.bombusmod.xml.InvalidXmlException;
import org.bombusmod.xml.XMLEventListener;
import org.bombusmod.xml.XMLParser;
import org.bombusmod.xmpp.Jid;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import net.jabjab.connection.zlib.ZLibInputStream;
import net.jabjab.connection.zlib.ZLibOutputStream;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class JabberStream implements XMLEventListener {

    private Socket connection;
    public InputStream inpStream;
    private OutputStream outStream;

    private final Stack<JabberDataBlock> tagStack = new Stack<JabberDataBlock>();
    private JabberListener listener;
    public final List<JabberBlockListener> blockListeners = new ArrayList<JabberBlockListener>();
    private String server; // for ping
    public boolean pingSent;
    public boolean loggedIn;
    private boolean xmppV1;
    private String sessionId;
    public final List<String> outgoingQueries = new ArrayList<String>();

    /**
     * Constructor. Connects to the server and sends the jabber welcome message.
     */
    public JabberStream(Jid clientJid) throws IOException {

        server = clientJid.getServer();
        int dnsPort = 5222;
        DnsSrvResolver srv = new DnsSrvResolver();

        if (srv.getSrv(server, DnsSrvResolver.XMPP_TCP)) {
            server = srv.getHost();
            dnsPort = srv.getPort();
        }

        connection = new Socket(server, dnsPort);

        try {
            connection.setKeepAlive(true);
            connection.setSoLinger(true, 300);
        } catch (Exception e) {
            e.printStackTrace();
        }
        inpStream = connection.getInputStream();
        outStream = connection.getOutputStream();
    }

    public void initiateStream() throws IOException, SAXException {
        blockListeners.clear();
        StringBuffer header = new StringBuffer("<stream:stream to='").append(server).append("' xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams'");
        header.append(" version='1.0'");
        /* TODO:
        if ( != null) {
            header.append(" xml:lang='").append(SR.MS_XMLLANG).append("'");
        }
        */
        header.append('>');
        sendPacket(header.toString());
        loop();
    }

    public void setStreamCompression() {
        try {
            inpStream = new ZLibInputStream(inpStream);
            outStream = new ZLibOutputStream(outStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setTls() throws IOException {
        final SSLSocketFactory sslFactory =
                SSLCertificateSocketFactory.getInsecure(20000, null);


        SSLSocket tls = (SSLSocket) sslFactory.createSocket(connection, server, connection.getPort(), true);
        tls.setUseClientMode(true);
        inpStream = tls.getInputStream();
        outStream = tls.getOutputStream();
    }

    public boolean tagStart(String name, String namespaceUri, Attributes attributes) {
        JabberDataBlock in;

        if (name.equals("message")) {
            in = new Message(attributes);
        } else if (name.equals("iq")) {
            in = new Iq(attributes);
        } else if (name.equals("presence")) {
            in = new Presence(attributes);
        } else if (name.equals("xml")) {
            return false;
        } else {
            in = new JabberDataBlock(name, namespaceUri, attributes);
        }

        if (name.equals("stream")) {
            sessionId = XMLParser.extractAttribute("id", attributes);
            String version = XMLParser.extractAttribute("version", attributes);
            xmppV1 = ("1.0".equals(version));

            broadcastBeginConversation();
            return false;
        }
        tagStack.push(in);

        return (name.equals("BINVAL"));
    }

    public boolean isXmppV1() {
        return xmppV1;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void tagEnd(String name) throws SAXException {
        if (tagStack.empty()) {
            if (name.equals("stream")) {
                throw new EndOfStreamException();
            } else {
                throw new InvalidXmlException();
            }
        }
        JabberDataBlock in = tagStack.pop();
        if (tagStack.empty()) {
            dispatchXmppStanza(in);
            if (in.getTagName().equals("error")) {
                System.out.println("Stream error: " + in.getText());
                close();
            }
        } else {
            tagStack.peek().addChild(in);
        }
    }

    protected void dispatchXmppStanza(JabberDataBlock dataBlock) {
        System.out.println("In: " + dataBlock.toString());
        if (dataBlock != null) {
            try {
                if (dataBlock instanceof Iq) {
                    // verify id attribute
                    if (dataBlock.getAttribute("id") == null) {
                        dataBlock.setAttribute("id", "666");
                    }
                    // verify is it our query
                    String type = dataBlock.getTypeAttribute();
                    if (type.equals("result") || type.equals("error")) {
                        String id = dataBlock.getAttribute("id");
                        if (outgoingQueries.indexOf(id) >= 0) {
                            outgoingQueries.remove(id);
                        } else {
                            return; // ignore bad iq result/error
                        }
                    }
                }

                int processResult = JabberBlockListener.BLOCK_REJECTED;
                for (JabberBlockListener blockListener : blockListeners) {
                    processResult = blockListener.blockArrived(dataBlock);
                    if (processResult == JabberBlockListener.BLOCK_PROCESSED) {
                        break;
                    }
                    if (processResult == JabberBlockListener.NO_MORE_BLOCKS) {
                        blockListeners.remove(blockListener);
                        break;
                    }
                }
                if (processResult == JabberBlockListener.BLOCK_REJECTED) {
                    if (dataBlock instanceof Iq) {
                        send(Iq.Error((Iq) dataBlock));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                listener.dispatcherException(e, dataBlock);
            }
        }
    }

    public void joinRoom(Jid roomJid, String nick) {
        Jid selfJid = new Jid(roomJid.getBare() + "/" + nick);
        System.out.println("Joining " + selfJid.toString());
        Presence muc = new Presence(Presence.PRESENCE_ONLINE, 0, null, nick);
        muc.setTo(selfJid.toString());
        JabberDataBlock x = new JabberDataBlock("x");
        x.setNameSpace("http://jabber.org/protocol/muc");
        JabberDataBlock history=x.addChild("history", null);
        history.setAttribute("maxstanzas", Integer.toString(20));
        history.setAttribute("maxchars","32768");
        muc.addChild(x);
        send(muc);
    }

    public void sendDeliveryMessage(Jid c, String id) {
        Message message = new Message(c.toString());
        // FIXME: no need to send <received /> to forwarded messages
        //xep-0184
        message.setAttribute("id", id); // FIXME: should be new id by XEP-0184 version 1.1
        message.addChildNs("received", "urn:xmpp:receipts").setAttribute("id", id);
        send(message);
    }


    private boolean connected = false;


    public void loop() {
        try {

            connected = true;

            SAXParserFactory spf = SAXParserFactory.newInstance();
            SAXParser sp = spf.newSAXParser();
            XMLReader xr = sp.getXMLReader();
            XMLParser dataHandler = new XMLParser(this);
            xr.setContentHandler(dataHandler);
            xr.parse(new InputSource(inpStream));

            //dispatcher.broadcastTerminatedConnection( null );
        }   catch (EndOfStreamException ex) {
            if (listener != null)
                listener.connectionClosed();
        }  catch (IOException ex) {
            if (listener != null)
                listener.connectionClosed();
        }  catch (Exception e) {
            e.printStackTrace();
            if (listener != null)
                listener.connectionTerminated(e);
        }
    }

    public void closeConnection() {

        try {
            sendPacket("</stream:stream>");
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        //setJabberListener(null);
        close();
    }

    /**
     * Method to close the connection to the server and tell the listener
     * that the connection has been terminated.
     */
    public void close() {
        connected = false;
        if (listener != null)
            listener.connectionClosed();
    }


    private void sendPacket(String data) throws IOException {
        if (outStream == null)
            return;
        synchronized (outStream) {
            outStream.write(data.getBytes());
            outStream.flush();
        }
    }


    private void sendBuf(StringBuffer data) throws IOException {
        System.out.println("Out: " + data.toString());
        sendPacket(data.toString());
    }

    /**
     * Method of sending a Jabber datablock to the server.
     *
     * @param block The data block to send to the server.
     */
    public void send(JabberDataBlock block) {
        if (block instanceof Iq) {
            String type = block.getTypeAttribute();
            if (type.equals("set") || type.equals("get")) {
                outgoingQueries.add(block.getAttribute("id"));
            }
        }
        try {
            StringBuffer buf = new StringBuffer();
            block.constructXML(buf);
            sendBuf(buf);
        } catch (Exception e) {
        }
    }

    /**
     * Set the listener to this stream.
     */
    public void addBlockListener(JabberBlockListener listener) {
        if (listener == null) {
            return;
        }
        if (blockListeners.indexOf(listener) > 0) {
            return;
        }
        blockListeners.add(listener);

    }

    public void cancelBlockListener(JabberBlockListener listener) {
        try {
            blockListeners.remove(listener);
        } catch (Exception e) {
        }
    }

    public void cancelBlockListenerByClass(Class removeClass) {
        int index = 0;
        int j = blockListeners.size();
        while (index < j) {
            Object list = blockListeners.get(index);
            if (list.getClass().equals(removeClass)) {
                blockListeners.remove(index);
                j--;
            } else {
                index++;
            }
        }

    }

    public void setJabberListener(JabberListener listener) {
        this.listener = listener;
    }

    /**
     * Method to tell the listener the stream is ready for talking to.
     */
    public void broadcastBeginConversation() {
        if (listener != null) {
            listener.beginConversation();
        }
    }


    public void plainTextEncountered(String text) {
        if (!tagStack.isEmpty()) {
            JabberDataBlock elem = tagStack.peek();
            elem.setText(elem.getText() + text);
        }
    }

    public void binValueEncountered(byte[] binvalue) {
        /*  if (!tagStack.isEmpty()) {
            //currentBlock.addText( text );
            ((JabberDataBlock) tagStack.peek()).addChild(binvalue);
        }*/
    }
}
