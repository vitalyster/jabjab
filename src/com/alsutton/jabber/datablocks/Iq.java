/*
  Copyright (c) 2000,2001 Al Sutton (al@alsutton.com)
  All rights reserved.
  Redistribution and use in source and binary forms, with or without modification, are permitted
  provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice, this list of
  conditions and the following disclaimer in the documentation and/or other materials provided with
  the distribution.

  Neither the name of Al Sutton nor the names of its contributors may be used to endorse or promote
  products derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
  THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.alsutton.jabber.datablocks;
import com.alsutton.jabber.*;
import org.bombusmod.xmpp.XmppError;
import org.xml.sax.Attributes;

import java.util.*;

/**
 * Class representing the iq message block
 */

public class Iq extends JabberDataBlock
{
    public final static int TYPE_SET=0;
    public final static int TYPE_GET=1;
    public final static int TYPE_RESULT=2;
    public final static int TYPE_ERROR=3;
    
  /**
   * Constructor including an Attribute list
   *
   * @param _attributes The list of element attributes
   */

  public Iq(Attributes _attributes )
  {
    super( "iq", null, _attributes );
  }
  
    public Iq(String to, int typeSet, String id) {
        super("iq");
        if (to != null && !to.isEmpty())
            setAttribute("to", to);
        String type;
        switch (typeSet) {
            case TYPE_SET:
                type = "set";
                break;
            case TYPE_GET:
                type = "get";
                break;
            case TYPE_ERROR:
                type = "error";
                break;
            default:
                type = "result";
        }
        setAttribute("type", type);
        setAttribute("id", id);
    }

    public static JabberDataBlock Error(Iq src) {
        String type = src.getTypeAttribute();
        if (type.equals("get") || type.equals("set")) {
            src.setAttribute("to", src.getAttribute("from"));
            src.setAttribute("from", null);
            src.setTypeAttribute("error");
            src.addChild(new XmppError(XmppError.FEATURE_NOT_IMPLEMENTED, null).construct());
        }
        return src;
    }
}
