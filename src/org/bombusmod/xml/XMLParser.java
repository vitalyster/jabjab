package org.bombusmod.xml;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class XMLParser extends DefaultHandler {


    private XMLEventListener eventListener;


    public XMLParser(XMLEventListener eventListener) {
        this.eventListener = eventListener;
    }

    @Override
    public void startElement(String namespaceURI, String localName, String qName, Attributes atts)
            throws SAXException {
        eventListener.tagStart(localName, namespaceURI, atts);
    }

    @Override
    public void endElement(String namespaceURI, String localName, String qName) throws SAXException {
        eventListener.tagEnd(localName);
    }

    @Override
    public void characters(char ch[], int start, int length) {
        eventListener.plainTextEncountered(new String(ch, start, length));
    }


    public static String extractAttribute(String attributeName, Attributes attributes) {
        return attributes.getValue(attributeName);
    }

}
