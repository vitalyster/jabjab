package org.bombusmod.xml;

import org.xml.sax.SAXException;

/**
 * Created with IntelliJ IDEA.
 * User: Vitaly
 * Date: 08.03.13
 * Time: 13:16
 * To change this template use File | Settings | File Templates.
 */
public class EndOfStreamException extends SAXException {
    public EndOfStreamException() {
        super("Stream closed");
    }
}
