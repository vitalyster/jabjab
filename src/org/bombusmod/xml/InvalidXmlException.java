package org.bombusmod.xml;

import org.xml.sax.SAXException;

/**
 * Created with IntelliJ IDEA.
 * User: Vitaly
 * Date: 08.03.13
 * Time: 13:19
 * To change this template use File | Settings | File Templates.
 */
public class InvalidXmlException extends SAXException {
    public InvalidXmlException() {
        super("Invalid XML");
    }
}
