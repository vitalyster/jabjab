/*
 * BookmarkQuery.java
 *
 * Created on 6.11.2006, 22:24
 * Copyright (c) 2005-2008, Eugene Stahov (evgs), http://bombus-im.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * You can also redistribute and/or modify this program under the
 * terms of the Psi License, specified in the accompanied COPYING
 * file, as published by the Psi Project; either dated January 1st,
 * 2005, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package org.bombusmod.xmpp.muc;

import com.alsutton.jabber.JabberBlockListener;
import com.alsutton.jabber.JabberDataBlock;
import com.alsutton.jabber.datablocks.Iq;

import java.util.*;

import com.alsutton.jabber.datablocks.Presence;
import net.jabjab.R;
import net.jabjab.client.XmppClient;
import net.jabjab.client.muc.BookmarkItem;


//#ifdef PRIVACY
//# import PrivacyLists.QuickPrivacy;
//#endif

/**
 * @author Evg_S
 */
public class BookmarkQuery implements JabberBlockListener {

    public final static boolean SAVE = true;
    public final static boolean LOAD = false;
    boolean queryType;

    XmppClient client;

    /**
     * Creates a new instance of BookmarkQurery
     */
    public BookmarkQuery(XmppClient client, boolean saveBookmarks) {
        this.queryType = saveBookmarks;
        this.client = client;
        JabberDataBlock request = new Iq(null, (saveBookmarks) ? Iq.TYPE_SET : Iq.TYPE_GET, "getbookmarks");
        JabberDataBlock query = request.addChildNs("query", "jabber:iq:private");

        JabberDataBlock storage = query.addChildNs("storage", "storage:bookmarks");
        /*if (saveBookmarks) {
            for (BookmarkItem b  : client.roster.bookmarks) {
                storage.addChild(b.constructBlock());
            }
        } */
        client.stream.send(request);
    }

    public int blockArrived(JabberDataBlock data) {
        if (data instanceof Iq) {
            if (data.getAttribute("id").equals("getbookmarks")) {
                String type = data.getTypeAttribute();
                if (type.equals("result")) {
                    JabberDataBlock query = data.findNamespace("query", "jabber:iq:private");
                    if (query != null) {
                        JabberDataBlock storage = query.findNamespace("storage", "storage:bookmarks");
                        if (storage != null) {
                            boolean autojoin = false;//cf.autoJoinConferences && sd.roster.myStatus != Presence.PRESENCE_INVISIBLE;

                            List<JabberDataBlock> items = storage.getChildBlocks();
                            if (items != null) {
                                for (Iterator<JabberDataBlock> blocks = items.iterator(); blocks.hasNext(); ) {
                                    BookmarkItem bm = new BookmarkItem(blocks.next());
                                    if (bm.nick == null) {
                                        bm.nick = client.account.JID.getNode();
                                    }

                                    if (bm.getName() == null) {
                                        bm.setName(bm.getJid().toString());
                                    }
                                    bm.image = R.drawable.ic_muc_offline;
                                    client.itemUpdated(bm, bm.getJid(), bm.getName(), Presence.PRESENCE_OFFLINE, "Offline", "Conference");
                                    if (queryType == LOAD) {
                                        if (bm.autojoin && autojoin) {
                                            // ConferenceForm.join(bm.name, bm.jid + '/' + bm.nick, bm.password, bm.nick, cf.confMessageCount);
                                        }
                                    }
                                }

                            }

                            if (items.isEmpty()) {
                                //loadDefaults(bookmarks);
                            }

                            return JabberBlockListener.NO_MORE_BLOCKS;
                        }
                    }
                }
            }
        }
        return JabberBlockListener.BLOCK_REJECTED;
    }
}
