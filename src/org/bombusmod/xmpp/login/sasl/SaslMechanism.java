/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bombusmod.xmpp.login.sasl;

import com.alsutton.jabber.JabberDataBlock;
import org.bombusmod.xmpp.Jid;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author Vitaly
 */
public abstract class SaslMechanism {
    protected Jid jid;
    protected String pass;

    public abstract String getName();
    public abstract String init(Jid Jid, String password) throws UnsupportedEncodingException;
    public abstract String response(String challenge) throws Exception;
    public abstract boolean success(String success);
    
    public static List<String> parseMechanisms(JabberDataBlock mechanisms) {
        List<String> result = new ArrayList<String>();
        for (JabberDataBlock mech : mechanisms.getChildBlocks()) {
            result.add(mech.getText());
        }
        return result;
    }
}
