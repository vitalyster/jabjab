/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bombusmod.xmpp.login.sasl;

import org.bombusmod.xmpp.Account;
import org.bombusmod.xmpp.login.sasl.mechanisms.SaslDigestMd5;
import org.bombusmod.xmpp.login.sasl.mechanisms.SaslGoogleToken;
import org.bombusmod.xmpp.login.sasl.mechanisms.SaslPlain;
import org.bombusmod.xmpp.login.sasl.mechanisms.SaslScramSha1;

import java.util.List;

/**
 *
 * @author Vitaly
 */
public class SaslFactory {
    public final static String NS_SASL = "urn:ietf:params:xml:ns:xmpp-sasl";
    public static SaslMechanism getPreferredMechanism(Account account, List<String> availableMechanisms) {
        if (availableMechanisms.contains("X-GOOGLE-TOKEN")) {
            account.isGoogle = true;
            return new SaslGoogleToken();
        }
        if (availableMechanisms.contains("SCRAM-SHA-1")) {
            return new SaslScramSha1();
        }
        if (availableMechanisms.contains("DIGEST-MD5")) {
            return new SaslDigestMd5();
        }
        if (availableMechanisms.contains("PLAIN") && account.plainAuth) {
            return new SaslPlain();
        }

        return null;
    }
}
