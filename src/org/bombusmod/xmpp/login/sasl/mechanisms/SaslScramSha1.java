package org.bombusmod.xmpp.login.sasl.mechanisms;

import org.bombusmod.util.Strconv;
import org.bombusmod.xmpp.Jid;
import org.bombusmod.xmpp.login.sasl.SaslMechanism;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.util.Random;


public class SaslScramSha1 extends SaslMechanism {

    String cnonce;
    String clientFirstMessageBare;
    String lServerSignature;
    Mac hmac;

    public String init(Jid jid, String password) {
        this.jid = jid;
        this.pass = password;
        calculateClientFirstMessage();
        return "n,," + clientFirstMessageBare;
    }

    public String response(String challenge) {
        String serverFirstMessage = challenge;
        String clientFinalMessage = "";
        try {
            clientFinalMessage = processServerMessage(serverFirstMessage);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return clientFinalMessage;
    }

    public boolean success(String success) {
        return lServerSignature.equals(success);
    }

    public SaslScramSha1() {
         try {
             hmac = Mac.getInstance("HmacSHA1");
         } catch (Exception e) {
             // TODO Auto-generated catch block
             e.printStackTrace();
         }
    }

    private void xorB(byte[] dest, byte[] source) {
        int l = dest.length;
        for (int i = 0; i < l; i++) {
            dest[i] ^= source[i];
        }
    }

    private String getAttribute(String[] attrs, char id) {
        for (int i = 0; i < attrs.length; i++) {
            String s = attrs[i].toString();
            if (s.charAt(0) == id) {
                return s.substring(2);
            }
        }
        return null;
    }

    protected void calculateClientFirstMessage() {
        Random rnd = new Random(System.currentTimeMillis());
        cnonce = "666" + rnd.nextLong();
        String username = jid.getNode();
        clientFirstMessageBare = "n=" + username + ",r=" + cnonce;
    }

 private String processServerMessage(String serverFirstMessage) {
 		String[] attrs = serverFirstMessage.split(",");

 		int i=Integer.parseInt( getAttribute(attrs, 'i') );
 		String salt=getAttribute(attrs, 's');
 		String r=getAttribute(attrs, 'r');
 		byte[] pwd;
 		try {
 			pwd = pass.getBytes("UTF-8");
 		} catch (UnsupportedEncodingException e) {
 			e.printStackTrace();
 			return null;
 		}

 		byte[] saltedPassword = Hi(pwd, Strconv.fromBase64(salt), i);
 		byte[] clientKey = getHMAC(saltedPassword).doFinal( "Client Key".getBytes() );
 		MessageDigest sha;
 		try {
 			sha = MessageDigest.getInstance("SHA-1");
 		} catch (Exception e) { return null; }

 		byte[] storedKey = sha.digest(clientKey);
 		String clientFinalMessageWithoutProof = "c=biws,r="+r;
 		String authMessage = clientFirstMessageBare + ","
 		                   + serverFirstMessage + ","
 				           + clientFinalMessageWithoutProof;
 		byte[] clientSignature = getHMAC(storedKey).doFinal( authMessage.getBytes() );
 		byte[] clientProof = clientKey.clone();
 		xorB(clientProof, clientSignature);


 		byte[] serverKey = getHMAC(saltedPassword).doFinal("Server Key".getBytes());


 		byte[] serverSignature = getHMAC(serverKey).doFinal(authMessage.getBytes());


 		lServerSignature = "v=" + Strconv.toBase64(serverSignature, serverSignature.length);


 		return clientFinalMessageWithoutProof + ",p=" + Strconv.toBase64(clientProof, clientProof.length);


 	}

     private Mac getHMAC(byte[] str) {
         try {
             SecretKeySpec secret = new SecretKeySpec(str, "HmacSHA1");
             hmac.init(secret);
         } catch (Exception e) {
             e.printStackTrace();
         }
         return hmac;
     }
     private byte[] Hi(byte[] str, byte[] salt, int i) {
         byte[] dest;


         Mac hmac = getHMAC(str);


         hmac.update(salt);


         //INT(1), MSB first
         hmac.update((byte) 0);
         hmac.update((byte) 0);
         hmac.update((byte) 0);
         hmac.update((byte) 1);


         byte[] U = hmac.doFinal();


         dest = U.clone();


         i--;


         while (i > 0) {
             U = hmac.doFinal(U);
             xorB(dest, U);
             i--;
         }


         return dest;
     }



    public String getName() {
        return "SCRAM-SHA-1";
    }
}
