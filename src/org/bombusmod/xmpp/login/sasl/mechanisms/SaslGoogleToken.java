/*
 * GoogleTokenAuth.java
 *
 * Created on 9.08.2008, 18.47
 *
 * Copyright (c) 2005-2007, Eugene Stahov (evgs), http://bombus-im.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * You can also redistribute and/or modify this program under the
 * terms of the Psi License, specified in the accompanied COPYING
 * file, as published by the Psi Project; either dated January 1st,
 * 2005, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.bombusmod.xmpp.login.sasl.mechanisms;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.bombusmod.util.StringUtils;
import org.bombusmod.xmpp.Jid;
import org.bombusmod.xmpp.login.sasl.SaslMechanism;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 *
 * @author root
 */
public class SaslGoogleToken extends SaslMechanism {

    public String init(Jid jid, String password) {
        this.jid = jid;
        pass = password;
        try {
            String firstUrl = "https://www.google.com:443/accounts/ClientAuth?Email="
                    + StringUtils.urlPrep(new String(jid.getBare().getBytes("UTF-8")))
                    + "&Passwd=" + StringUtils.urlPrep(new String(password.getBytes("UTF-8")))
                    + "&PersistentCookie=false&source=googletalk";
            //log.addMessage("Connecting to www.google.com");
            HttpClient c = new DefaultHttpClient();
            HttpGet request = new HttpGet(firstUrl.toString());
            HttpResponse response = c.execute(request);
            InputStream in = response.getEntity().getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String sid = reader.readLine();
            if (!sid.startsWith("SID=")) {
                throw new SecurityException();
            }

            String lsid = reader.readLine();

            String secondUrl = "https://www.google.com:443/accounts/IssueAuthToken?"
                    + sid + "&" + lsid + "&service=mail&Session=true";
            HttpGet request2 = new HttpGet(secondUrl);

            HttpResponse response2 = c.execute(request2);
            InputStream in2 = response2.getEntity().getContent();
            BufferedReader reader2 = new BufferedReader(new InputStreamReader(in2));
            StringBuffer token = new StringBuffer();
            token.append((char) 0).append(new String(jid.getNode().getBytes("UTF-8")))
                    .append((char) 0).append(reader2.readLine());
            return token.toString();

        } catch (SecurityException e) {
            throw e;
        } catch (Exception e) {
            //listener.loginFailed("Google token error");
        }
        return null;
    }

    public String response(String challenge) {
        return "";
    }

    public String getName() {
        return "X-GOOGLE-TOKEN";
    }

    public boolean success(String success) {
        return true;
    }
}
