/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.bombusmod.xmpp.login.sasl.mechanisms;

import org.bombusmod.xmpp.Jid;
import org.bombusmod.xmpp.login.sasl.SaslMechanism;

import java.io.UnsupportedEncodingException;

/**
 * @author Vitaly
 */
public class SaslPlain extends SaslMechanism {

    public String getName() {
        return "PLAIN";
    }

    public String init(Jid Jid, String password) throws UnsupportedEncodingException {
        this.jid = Jid;
        pass = password;
        StringBuilder result = new StringBuilder();
        result.append(new String(jid.getBare().getBytes("UTF-8")))
                .append((char) 0x00)
                .append(new String(jid.getNode().getBytes("UTF-8")))
                .append((char) 0x00)
                .append(new String(password.getBytes("UTF-8")));
        return result.toString();
    }

    public String response(String challenge) throws Exception {
        return "";
    }

    public boolean success(String success) {
        return true;
    }

}
