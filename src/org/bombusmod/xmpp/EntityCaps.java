/*
 * EntityCaps.java
 *
 * Created on 17 Июнь 2007 г., 2:58
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package org.bombusmod.xmpp;

import com.alsutton.jabber.JabberBlockListener;
import com.alsutton.jabber.JabberDataBlock;
import com.alsutton.jabber.datablocks.Iq;
import net.jabjab.client.XmppClient;
import org.bombusmod.util.Strconv;

import java.security.DigestException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Evg_S
 */
public class EntityCaps implements JabberBlockListener {

    XmppClient client;

    /**
     * Creates a new instance of EntityCaps
     */
    public EntityCaps(XmppClient client) {
        this.client = client;
        initCaps();
    }

    public int blockArrived(JabberDataBlock data) {
        if (!(data instanceof Iq)) return BLOCK_REJECTED;
        if (!data.getTypeAttribute().equals("get")) return BLOCK_REJECTED;

        JabberDataBlock query = data.findNamespace("query", "http://jabber.org/protocol/disco#info");
        if (query == null) return BLOCK_REJECTED;
        String node = query.getAttribute("node");

        if (node != null)
            try {
                if (!node.equals(BOMBUS_NAMESPACE + "#" + calcVerHash()))
                    return BLOCK_REJECTED;
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                return BLOCK_REJECTED;
            } catch (DigestException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                return BLOCK_REJECTED;
            }

        JabberDataBlock result = new Iq(data.getAttribute("from"), Iq.TYPE_RESULT, data.getAttribute("id"));
        result.addChild(query);

        JabberDataBlock identity = query.addChild("identity", null);
        identity.setAttribute("category", BOMBUS_ID_CATEGORY);
        identity.setAttribute("type", BOMBUS_ID_TYPE);
        identity.setAttribute("name", "BombusMod");

        int j = features.size();
        for (int i = 0; i < j; i++) {
            query.addChild("feature", null).setAttribute("var", features.get(i));
        }

        client.stream.send(result);

        return BLOCK_PROCESSED;
    }

    public static String ver = null;

    static String calcVerHash() throws NoSuchAlgorithmException, DigestException {
        if (ver != null) {
            return ver;
        }
        int j = features.size();
        if (j < 1) {
            initCaps();
        }

        MessageDigest sha1 = MessageDigest.getInstance("SHA-1");

        //indentity
        StringBuffer identity = new StringBuffer();
        identity.append(BOMBUS_ID_CATEGORY).append("/").append(BOMBUS_ID_TYPE)
                .append("//").append("BombusMod").append("<");

        for (int i = 0; i < j; i++) {
            identity.append((String) features.get(i));
            identity.append("<");
        }
        sha1.update(identity.toString().getBytes(), 0, identity.length());
        byte[] sha1bits = new byte[20];
        sha1.digest(sha1bits, 0, sha1bits.length);
        ver = Strconv.toBase64(sha1bits, sha1bits.length);

        return ver;

    }

    public static JabberDataBlock presenceEntityCaps() {
        try {
            JabberDataBlock c = new JabberDataBlock("c");
            c.setAttribute("xmlns", "http://jabber.org/protocol/caps");
            c.setAttribute("node", BOMBUS_NAMESPACE);//+'#'+Version.getVersionNumber());
            c.setAttribute("ver", calcVerHash());
            c.setAttribute("hash", "sha-1");
            return c;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (DigestException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;
    }

    private final static String BOMBUS_NAMESPACE = "http://bombusmod.net.ru/caps";
    private final static String BOMBUS_ID_CATEGORY = "client";
    private final static String BOMBUS_ID_TYPE = "mobile";

    static void initCaps() {
        features.clear();
        features.add("http://jabber.org/protocol/disco#info");
        features.add("http://jabber.org/protocol/disco#items");
        features.add("jabber:iq:last");
        features.add("jabber:iq:privacy");
        features.add("jabber:iq:roster");
        features.add("jabber:iq:time"); //DEPRECATED
        features.add("jabber:iq:version");
        features.add("jabber:x:oob");
        features.add("urn:xmpp:ping");
        features.add("urn:xmpp:receipts"); //xep-0184
        features.add("urn:xmpp:time");

        //sort(features);
    }

    private static final List<String> features = new ArrayList<String>();
}
