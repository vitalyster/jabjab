/*
 * IqQueryRoster.java
 *
 * Created on 12.01.2005, 0:17
 *
 * Copyright (c) 2005-2008, Eugene Stahov (evgs), http://bombus-im.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * You can also redistribute and/or modify this program under the
 * terms of the Psi License, specified in the accompanied COPYING
 * file, as published by the Psi Project; either dated January 1st,
 * 2005, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.bombusmod.xmpp;

import com.alsutton.jabber.*;
import com.alsutton.jabber.datablocks.*;

import java.util.List;
import net.jabjab.client.RosterItem;
import net.jabjab.client.XmppClient;

public class RosterDispatcher implements JabberBlockListener {

    public static final String NS_ROSTER = "jabber:iq:roster";


    XmppClient client;

    public RosterDispatcher(XmppClient client) {
        this.client = client;
    }

    public static Iq QueryRoster() {
        Iq result = new Iq(null, Iq.TYPE_GET, "getros");

        result.addChildNs("query", NS_ROSTER);
        return result;        
    }

    /** add to roster*/
    public static Iq QueryRoster(Jid jid, String name, String group, String subscription) {
        Iq result = new Iq(null, Iq.TYPE_SET, "addros");
        JabberDataBlock qB = result.addChildNs("query", NS_ROSTER);
        JabberDataBlock item = qB.addChild("item", null);
        item.setAttribute("jid", jid.getBare());
        if (name != null) {
            item.setAttribute("name", name);
        }
        if (subscription != null)
        item.setAttribute("subscription", subscription);
        if (group != null) {
            item.addChild("group", group);
        }
        return result;
    }

    public int blockArrived(JabberDataBlock data) {
        if (data instanceof Iq) {
            String from = data.getAttribute("from");
            String type = data.getTypeAttribute();
            String id = data.getAttribute("id");

            if (type.equals("result")) {
                if (id.equals("getros")) {
                    //theStream.enableRosterNotify(false); //voffk

                    if (!processRoster(data)) {
                        return JabberBlockListener.BLOCK_REJECTED;
                    }
                    return JabberBlockListener.BLOCK_PROCESSED;
                }
            } else if (type.equals("set")) {
                if (processRoster(data)) {
                    client.stream.send(new Iq(from, Iq.TYPE_RESULT, id));
                    return JabberBlockListener.BLOCK_PROCESSED;
                }
            }
        }
        return BLOCK_REJECTED;
    }

    boolean processRoster(JabberDataBlock data) {
        JabberDataBlock q = data.findNamespace("query", NS_ROSTER);
        if (q != null) {
        /*
        //verifying from attribute as in RFC3921/7.2
        String from = data.getAttribute("from");
        if (from != null) {
            JID fromJid = new JID(from);
            if (!fromJid.resource.equals("")) {
                if (!client.jid.equals(fromJid)) {
                    return false;
                }
            }
        }
          */

            List<JabberDataBlock> items = q.getChildBlocks();
            if (items != null) {
                for (JabberDataBlock item : items) {
                    if (item.getTagName().equals("item")) {
                        String name = item.getAttribute("name");
                        String jid = item.getAttribute("jid");
                        String subscr = item.getAttribute("subscription");
                        boolean ask = (item.getAttribute("ask") != null);
                        String group = item.getChildBlockText("group");

                        client.itemUpdated(new RosterItem(), new Jid(jid), name,
                                Presence.PRESENCE_OFFLINE, "Offline", group);
                    }
                }
            }
            return true;
        }
        return false;
    }
}
