package org.bombusmod.xmpp;

import net.jabjab.client.RosterItem;

/**
 * Created with IntelliJ IDEA.
 * User: Vitaly
 * Date: 16.06.12
 * Time: 16:13
 * To change this template use File | Settings | File Templates.
 */
public interface RosterListener {

    public void itemUpdated(RosterItem c, Jid rosterItem, String nick, int status, String statusText, String groups);

}
