package org.bombusmod.xmpp;

import android.util.Log;
import com.alsutton.jabber.JabberBlockListener;
import com.alsutton.jabber.JabberDataBlock;
import com.alsutton.jabber.datablocks.Message;
import com.alsutton.jabber.datablocks.Presence;
import net.jabjab.client.RosterItem;
import net.jabjab.client.XmppClient;
import net.jabjab.client.muc.BookmarkItem;
import org.bombusmod.util.Time;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Vitaly
 * Date: 23.06.12
 * Time: 15:02
 * To change this template use File | Settings | File Templates.
 */
public class MessageDispatcher implements JabberBlockListener {

    XmppClient client;

    public MessageDispatcher(XmppClient client) {
        this.client = client;
    }

    @Override
    public int blockArrived(JabberDataBlock data) {
        if (data instanceof Message) {
            Jid fromJid = new Jid(((Message) data).getFrom());
            String type = data.getTypeAttribute();
            if (type == null)
                type = "chat";
            boolean gc = type.equals("groupchat");
            String mucFrom = gc ? fromJid.resource : null;
            String msg = ((Message) data).getBody().trim();
            if (type.equals("chat")) {
                if (data.findNamespace("request", "urn:xmpp:receipts") != null) {
                    client.stream.sendDeliveryMessage(fromJid, data.getAttribute("id"));
                }
                JabberDataBlock received = data.findNamespace("received", "urn:xmpp:receipts");
                if (received != null) {
                    client.messageDelivered(fromJid, data.getAttribute("id")); //FIXME: compatibilty with XEP-0184 version 1.0
                    client.messageDelivered(fromJid, received.getAttribute("id")); // XEP-0184 Version 1.1
                }
            }
            if (msg == null || msg.isEmpty()) {
                return BLOCK_PROCESSED;
            }
            Msg message = new Msg();
            message.setId(data.getAttribute("id"));
            message.setFrom(new Jid(fromJid.getBare()));
            message.setBody(msg);
            message.setGroupChat(gc);
            message.setSubject(message.isGroupChat() ? mucFrom : fromJid.getNode());
            message.setOutgoing(false);
            JabberDataBlock delay = data.findNamespace("delay", "urn:xmpp:delay");
            if (delay != null) {
                message.setTimeStamp(Time.dateIso8601(delay.getAttribute("stamp")));
            } else {
                message.setTimeStamp(System.currentTimeMillis());
            }
            client.messageReceived(new Jid(fromJid.getBare()), message);
        }
        return JabberBlockListener.BLOCK_REJECTED;
    }

    public interface MessageListener {
        void messageReceived(Msg message);
    }
}
