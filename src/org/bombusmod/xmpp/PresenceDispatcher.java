package org.bombusmod.xmpp;

import com.alsutton.jabber.JabberBlockListener;
import com.alsutton.jabber.JabberDataBlock;
import com.alsutton.jabber.datablocks.Presence;
import net.jabjab.client.RosterItem;
import net.jabjab.client.XmppClient;
import net.jabjab.client.muc.MucContact;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Vitaly
 * Date: 23.06.12
 * Time: 14:19
 * To change this template use File | Settings | File Templates.
 */
public class PresenceDispatcher implements JabberBlockListener {

    XmppClient client;

    public PresenceDispatcher(XmppClient client) {
        this.client = client;
    }

    @Override
    public int blockArrived(JabberDataBlock data) {
        if (data instanceof Presence) {
            ((Presence) data).dispathch();
            int ti = ((Presence) data).getTypeIndex();
            Jid contact = new Jid(((Presence) data).getFrom());
            String statusText = ((Presence) data).getStatus();

            JabberDataBlock xmuc = data.findNamespace("x", "http://jabber.org/protocol/muc#user");
            if (xmuc == null) {
                xmuc = data.findNamespace("x", "http://jabber.org/protocol/muc"); //join errors
            }
            if (xmuc != null) {
                RosterItem rosterItem = client.getItem(contact);
                if (rosterItem != null) {
                    List<MucContact> mucContacts = rosterItem.getSubcontacts();
                    if (Presence.PRESENCE_OFFLINE == ti) {
                        for (int i = 0; i < mucContacts.size(); ++i) {
                            MucContact mc = mucContacts.get(i);
                            if (mc.getNick().equals(contact.resource)) {
                                mc.setStatus(Presence.PRESENCE_OFFLINE);
                                mc.setStatusText(null);
                                mucContacts.remove(i);
                            }
                        }
                        if (mucContacts.size() == 0) mucContacts.clear();
                    } else {
                        MucContact c = getMucContact(mucContacts, contact);
                        if (c == null)
                            c = new MucContact(contact.resource, contact);
                        mucContacts.remove(c);
                        c.setStatus(Presence.PRESENCE_OFFLINE);
                        JabberDataBlock item = xmuc.getChildBlock("item");
                        String aJid = item.getAttribute("jid");
                        Jid tempRealJid = aJid == null ? null : new Jid(aJid);
                        c.realJid = tempRealJid;
                        c.setStatus(ti);
                        c.setStatusText(statusText);
                        mucContacts.add(c);
                    }
                    client.itemUpdated(null, contact, contact.getNode(), Presence.PRESENCE_ONLINE, "", null);
                }
            } else {
                String nick = null;
                // XEP-0172
                JabberDataBlock nickData = data.findNamespace("nick", "http://jabber.org/protocol/nick");
                if (nickData != null) {
                    nick = nickData.getText();
                }
                client.itemUpdated(null, contact, nick, ti, statusText, null);
            }
            return JabberBlockListener.BLOCK_PROCESSED;
        };
        return JabberBlockListener.BLOCK_REJECTED;
    }

    public MucContact getMucContact(List<MucContact> mucContacts, Jid contact) {
        for (int i = 0; i < mucContacts.size(); ++i) {
            MucContact mc = mucContacts.get(i);
            if (mc.getNick().equals(contact.resource)) {
                return mc;
            }
        }
        return null;
    }
}
