package org.bombusmod.xmpp;

import org.bombusmod.util.Time;

/**
 * Created with IntelliJ IDEA.
 * User: Vitaly
 * Date: 10.01.13
 * Time: 2:00
 * To change this template use File | Settings | File Templates.
 */
public class Msg {

    private String id;

    private long timeStamp;

    private Jid from;

    private String subject;

    private String body;

    private boolean groupChat;

    private boolean outgoing;

    private boolean delivered;

    private boolean unread = false;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Jid getFrom() {
        return from;
    }

    public void setFrom(Jid from) {
        this.from = from;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public boolean isGroupChat() {
        return groupChat;
    }

    public void setGroupChat(boolean groupChat) {
        this.groupChat = groupChat;
    }

    public boolean isOutgoing() {
        return outgoing;
    }

    public void setOutgoing(boolean outgoing) {
        this.outgoing = outgoing;
    }

    public String getTime() {
        if (Time.utcTimeMillis() - getTimeStamp() > (24 * 60 * 60 * 1000L)) return getDayTime();
        return Time.timeLocalString(getTimeStamp());
    }

    public String getDayTime() {
        return Time.dayLocalString(getTimeStamp()) + Time.timeLocalString(getTimeStamp());
    }

    public boolean isDelivered() {
        return delivered;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }
}
