package net.jabjab.service;

import android.os.AsyncTask;
import com.alsutton.jabber.JabberStream;
import net.jabjab.client.XmppClient;
import org.xml.sax.SAXException;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Vitaly
 * Date: 18.06.12
 * Time: 9:26
 * To change this template use File | Settings | File Templates.
 */
public class NetworkTask extends AsyncTask<XmppClient, Void, Void> {
    @Override
    protected Void doInBackground(XmppClient... client) {
        try {

            client[0].stream = client[0].account.openJabberStream();
            client[0].stream.setJabberListener(client[0]);
            client[0].stream.initiateStream();
        }   catch (SAXException e) {
            // end of stream or invalid xml
        }   catch (Exception e) {
            e.printStackTrace();
        }
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
