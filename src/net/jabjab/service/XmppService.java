package net.jabjab.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import net.jabjab.R;
import net.jabjab.client.RosterItem;
import net.jabjab.client.XmppClient;
import net.jabjab.receivers.NetworkStateReceiver;
import net.jabjab.activities.MainActivity;
import org.bombusmod.xmpp.Msg;

/**
 * Created with IntelliJ IDEA.
 * User: Vitaly
 * Date: 16.06.12
 * Time: 9:46
 * To change this template use File | Settings | File Templates.
 */
public class XmppService extends Service implements XmppClient.NotifyListener {

    protected final LocalBinder localBinder = new LocalBinder();

    public XmppClient client;

    public static String shortVersion;

    public static String clientVersion;


    NetworkStateReceiver networkReceiver;

    private NotificationManager mNM;
    private ServiceHelper helper;

    @Override
    public void onCreate() {
        super.onCreate();
        shortVersion = getString(R.string.version);
        clientVersion = String.format("%sgit-%s", getString(R.string.version), getString(R.string.git));
        client = new XmppClient();
        client.notifyListener = this;
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        networkReceiver = new NetworkStateReceiver();
        registerReceiver(networkReceiver, filter);

        mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        //Foreground Service
        helper = new ServiceHelper(this, mNM);
        NotificationCompat.Builder notification = new NotificationCompat.Builder(this);
        notification.setSmallIcon(R.drawable.app_service);
        notification.setContentTitle(getString(R.string.app_name));
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
        notification.setContentIntent(contentIntent);
        helper.startForegroundCompat(R.string.app_name, notification.getNotification());
    }

    @Override
    public IBinder onBind(Intent intent) {
        return localBinder;
    }


    public static int NOTIFY_MESSAGE = 1;

    @Override
    public void onNotify(RosterItem item, Msg message) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);
        int icon = R.drawable.inc_msg;
        CharSequence tickerText = String.format("%s: %s", item.getName(), message.getBody());
        long when = System.currentTimeMillis();
        long[] vibraPattern = { 0, 500, 250, 500 };
        NotificationCompat.Builder notification = new NotificationCompat.Builder(this);
        Context context = getApplicationContext();
        CharSequence contentTitle = item.getName();
        CharSequence contentText = message.getBody();
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setAction("net.jabjab.notify");
        notificationIntent.putExtra("jid", item.getJid().getBare());
        notificationIntent.putExtra("name", item.getName());
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_ONE_SHOT);
        notification.setLights(0xff00ff00, 300, 1000);
        notification.setVibrate(vibraPattern);
        notification.setDefaults(Notification.DEFAULT_SOUND);
        notification.setContentIntent(contentIntent);
        notification.setContentTitle(contentTitle);
        notification.setContentText(contentText);
        notification.setTicker(tickerText);
        notification.setSmallIcon(icon);

        mNotificationManager.notify(NOTIFY_MESSAGE, notification.getNotification());
    }

    public class LocalBinder extends Binder {
        public XmppService getService() {
            return XmppService.this;
        }
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(networkReceiver);
        helper.stopForegroundCompat(R.string.app_name);
        super.onDestroy();
    }


}
