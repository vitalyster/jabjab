package net.jabjab;

import android.app.backup.BackupAgentHelper;
import android.app.backup.FileBackupHelper;
import android.app.backup.SharedPreferencesBackupHelper;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created with IntelliJ IDEA.
 * User: Vitaly
 * Date: 05.11.12
 * Time: 17:17
 * To change this template use File | Settings | File Templates.
 */
public class SettingsBackupHelper extends BackupAgentHelper {
    // A key to uniquely identify the set of backup data
    static final String PREFS_BACKUP_KEY = "credentials";

    // Allocate a helper and add it to the backup agent
    @Override
    public void onCreate() {
        SharedPreferencesBackupHelper helper = new SharedPreferencesBackupHelper(this, JabJab.CREDS_PREFS);
        addHelper(PREFS_BACKUP_KEY, helper);
    }

}
