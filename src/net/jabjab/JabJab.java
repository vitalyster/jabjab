package net.jabjab;

import android.app.Activity;
import android.app.Application;
import android.app.backup.BackupManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import net.jabjab.client.XmppClient;
import net.jabjab.service.XmppService;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Vitaly
 * Date: 05.11.12
 * Time: 17:08
 * To change this template use File | Settings | File Templates.
 */
public class JabJab extends Application {

    public BackupManager backupManager;
    private static JabJab instance;
    private static XmppService service;
	public static final String CREDS_PREFS = "creds";

    public static JabJab getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        backupManager = new BackupManager(this);
    }

    public static void setService(XmppService s) {
        service = s;
    }

    public static XmppService getService() {
        return service;
    }
}
