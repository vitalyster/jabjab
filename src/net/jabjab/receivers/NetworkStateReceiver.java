/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.jabjab.receivers;

import android.content.*;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import android.util.Log;
import net.jabjab.JabJab;
import net.jabjab.client.XmppClient;
import net.jabjab.service.XmppService;
import org.bombusmod.xmpp.Account;
import org.bombusmod.xmpp.Jid;

/**
 *
 * @author Vitaly
 */
public class NetworkStateReceiver extends BroadcastReceiver {

    private boolean disconnected = false;

    @Override
    public void onReceive(Context context, Intent networkIntent) {
        XmppClient client = ((XmppService)context).client;
        SharedPreferences prefs = context.getSharedPreferences(JabJab.CREDS_PREFS, 0);
        if (networkIntent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false)) {
            if (!disconnected) {
                Log.d(NetworkStateReceiver.class.getSimpleName(), "Connectivity lost");
                disconnected = true;
                client.logout();
            }
        } else {
            if (!disconnected) {
                return;
            } else {
                disconnected = false;
                Log.d(NetworkStateReceiver.class.getSimpleName() , "Connected");
                Account acc = new Account();
                acc.JID = new Jid(prefs.getString("jid", ""));
                acc.password = prefs.getString("pass", "");
                client.login(acc);
            }
        }
    }
}
