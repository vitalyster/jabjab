package net.jabjab.views;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.alsutton.jabber.datablocks.Presence;
import net.jabjab.R;
import net.jabjab.activities.ChatActivity;
import net.jabjab.activities.MucUsersActivity;
import net.jabjab.client.Group;
import net.jabjab.client.RosterItem;
import net.jabjab.client.XmppClient;
import net.jabjab.client.muc.BookmarkItem;
import net.jabjab.models.RosterAdapter;
import org.bombusmod.xmpp.Jid;

/**
 * Created with IntelliJ IDEA.
 * User: Vitaly
 * Date: 23.06.12
 * Time: 9:39
 * To change this template use File | Settings | File Templates.
 */
public class RosterView extends Fragment {

    OnItemSelectedListener itemSelectedListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.roster_view, null);
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            itemSelectedListener = (OnItemSelectedListener) activity;
        } catch (ClassCastException e) {
            Log.e(getString(R.string.app_name), "Bad class", e);
            throw new ClassCastException(activity.toString()
                    + " must implement OnItemSelectedListener");
        }
    }

    private final RosterAdapter adapter = new RosterAdapter();
    private ArrayAdapter<String> groupsAdapter;

    public void viewRoster(final XmppClient client) {
        groupsAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item);
        adapter.setGroupsAdapter(groupsAdapter);
        groupsAdapter.add("All");
        groupsAdapter.add("Active");
        adapter.init(getActivity(), client);
        addBar();
        ListView lv = (ListView) getActivity().findViewById(R.id.roster_view);
        lv.setAdapter(adapter);
        lv.setTextFilterEnabled(true);
        lv.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long arg3) {
                TextView name = (TextView) view.findViewById(R.id.contactName);
                TextView jid = (TextView) view.findViewById(R.id.contactJid);
                itemSelectedListener.itemSelected(name.getText().toString(), new Jid(jid.getText().toString()));
            }
        });
    }

    private void addBar() {
        ActionBar bar = ((SherlockFragmentActivity)getActivity()).getSupportActionBar();
        bar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        groupsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        bar.setListNavigationCallbacks(groupsAdapter, new ActionBar.OnNavigationListener() {

            @Override
            public boolean onNavigationItemSelected(int position, long itemId) {
                adapter.setSelectedGroup(position);
                adapter.getGroupsAdapter().notifyDataSetChanged();
                adapter.notifyDataSetChanged();
                return false;
            }
        });
    }

    public RosterItem getItem(Jid jid) {
        return adapter.getItem(jid);
    }

    public void itemUpdated(RosterItem c, Jid item, String nick, int status, String statusText, String groups) {
        RosterItem contact = getItem(item);
        if (c != null) {
            contact = c;
            contact.setJid(item);
            if (nick != null && !nick.isEmpty())
                contact.setName(nick);
            else
                contact.setName(item.getBare());
            if (groups.length() == 0) {
                adapter.addItem("General", contact);
            } else {
                contact.addGroup(groups);
                adapter.addItem(contact);
            }
		}

		if (contact != null) {
            int oldStatus = contact.lastPresence;
		    contact.lastPresence = status;
            contact.setStatusText(statusText);
			if (c == null) {
                adapter.onUpdateContactList(contact, oldStatus);
                Update();
			}
		} else return;

        boolean gc = contact instanceof BookmarkItem;
        if (gc) {
            adapter.sortBookmarks(groups);
            String mucJid = contact.getJid().toString() + "/" + ((BookmarkItem)contact).nick;
            if (!item.toString().equals(mucJid)) // full jid compare
                return;
        }

        if (status != Presence.PRESENCE_AUTH_ASK) {
            if (status != Presence.PRESENCE_ERROR) {
                if (gc) {
                    switch (status) {
                        case Presence.PRESENCE_OFFLINE:
                            contact.image = R.drawable.ic_muc_offline;
                            break;
                        default:
                            contact.image = R.drawable.ic_muc_online;
                            break;
                    }
                } else {
                    switch (status) {
                        case Presence.PRESENCE_ONLINE:
                            contact.image = R.drawable.ic_item_online;
                            break;
                        case Presence.PRESENCE_OFFLINE:
                            contact.image = R.drawable.ic_item;
                            break;
                        case Presence.PRESENCE_AWAY:
                            contact.image = R.drawable.ic_item_away;
                            break;
                        case Presence.PRESENCE_CHAT:
                            contact.image = R.drawable.ic_item_chat;
                            break;
                        case Presence.PRESENCE_DND:
                            contact.image = R.drawable.ic_item_dnd;
                            break;
                        case Presence.PRESENCE_XA:
                            contact.image = R.drawable.ic_item_na;
                            break;
                        case Presence.PRESENCE_AUTH:
                            contact.image = R.drawable.ic_item_auth;
                            break;
                        case Presence.PRESENCE_INVISIBLE:
                            contact.image = R.drawable.ic_item_inv;
                            break;
                    }
                }
            }
        }
    }


    public void logOff() {
        for (int ii = 0; ii < adapter.groups.size(); ii++) {
            Group group = adapter.groups.get(ii);
            for (int i = 0; i < group.getRosterItems().size(); i++) {
                RosterItem contact = group.getRosterItems().get(i);
                contact.lastPresence = Presence.PRESENCE_OFFLINE;
                contact.setStatusText("Offline");
            }
            group.setOnlineCount(0);
            adapter.clearRoster(group);
        }
        Update();
    }

    public void Update() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }

    public static boolean showOnlyOnlines = false;

    public void toggleOnlines() {
        showOnlyOnlines = !showOnlyOnlines;
	    Update();
    }

    public void setProgress(int textId) {
        TextView tv = (TextView) getActivity().findViewById(R.id.progress_view);
        if (textId == 0) {
            tv.setVisibility(View.INVISIBLE);
        } else {
            tv.setVisibility(View.VISIBLE);
            tv.setText(textId);
        }
    }

    public interface OnItemSelectedListener {
        public void itemSelected(String name, Jid jid);
    }

}
