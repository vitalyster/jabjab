package net.jabjab.views;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import net.jabjab.JabJab;
import net.jabjab.R;

/**
 * Created with IntelliJ IDEA.
 * User: Vitaly
 * Date: 16.06.12
 * Time: 12:19
 * To change this template use File | Settings | File Templates.
 */
public class SignInView extends DialogFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.signin_form, container, false);
        getDialog().setTitle(R.string.ms_account_name);
        Button loginButton = (Button) v.findViewById(R.id.btn_login);
        final TextView unView = (TextView) v.findViewById(R.id.et_un);
        final TextView pwView = (TextView) v.findViewById(R.id.et_pw);

        SharedPreferences prefs = getActivity().getSharedPreferences(JabJab.CREDS_PREFS, 0);
        unView.setText(prefs.getString("jid", ""));
        pwView.setText(prefs.getString("pass", ""));

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userName = unView.getText().toString();
                String password = pwView.getText().toString();
                SharedPreferences prefs = getActivity().getSharedPreferences(JabJab.CREDS_PREFS, 0);
                SharedPreferences.Editor ed = prefs.edit();
                ed.putString("jid", userName);
                ed.putString("pass", password);
                ed.commit();
                JabJab.getInstance().backupManager.dataChanged();
                ((SignInViewListener)getActivity()).onFinishSignInView(userName, password);
                dismiss();
            }
        });
        return v;
    }

    public interface SignInViewListener {
        void onFinishSignInView(String jid, String pass);
    }
}