package net.jabjab.views;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import com.alsutton.jabber.datablocks.Presence;
import net.jabjab.R;
import net.jabjab.client.StatusItem;
import net.jabjab.models.StatusesAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Vitaly
 * Date: 03.07.12
 * Time: 21:28
 * To change this template use File | Settings | File Templates.
 */
public class StatusesView extends DialogFragment {
    StatusesAdapter statusesAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(R.string.ms_status_menu);
        View v = inflater.inflate(R.layout.statuses_view, container, false);
        StatusItem online = new StatusItem(Presence.PRESENCE_ONLINE);
        online.setStatusText("Online");
        StatusItem offline = new StatusItem(Presence.PRESENCE_OFFLINE);
        offline.setStatusText("Offline");
        List<StatusItem> statuses = new ArrayList<StatusItem>();
        statuses.add(0, online);
        statuses.add(1, offline);
        statusesAdapter = new StatusesAdapter(getActivity(), statuses);
        ListView lv = (ListView) v.findViewById(R.id.statuses_view);
        lv.setAdapter(statusesAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                StatusItem newStatus = statusesAdapter.getItem(i);
                StatusesViewListener activity = (StatusesViewListener) getActivity();
                activity.onChangeStatus(newStatus.getStatus());
                dismiss();
            }
        });
        return v;
    }

    public interface StatusesViewListener {
        void onChangeStatus(int newStatus);
    }
}