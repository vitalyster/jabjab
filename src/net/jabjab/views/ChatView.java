package net.jabjab.views;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.alsutton.jabber.datablocks.Message;
import com.alsutton.jabber.datablocks.Presence;
import net.jabjab.R;
import net.jabjab.client.RosterItem;
import net.jabjab.client.XmppClient;
import net.jabjab.client.muc.BookmarkItem;
import net.jabjab.models.MessagesAdapter;
import org.bombusmod.xmpp.Jid;
import org.bombusmod.xmpp.Msg;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created with IntelliJ IDEA.
 * User: Vitaly
 * Date: 23.06.12
 * Time: 9:53
 * To change this template use File | Settings | File Templates.
 */
public class ChatView extends Fragment implements AbsListView.OnScrollListener {

    private XmppClient client;
    MessagesAdapter messagesAdapter;
    private MyListView contactChat;
    private static Hashtable<String, Integer> positionHash = new Hashtable<String, Integer>();
    private String contactJid;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.chat, container, false);
    }

    private void addLastPosition(String jid, int position) {
        positionHash.put(jid, position);
    }

    private int getLastPosition(String jid) {
        if (positionHash.containsKey(jid)) return positionHash.remove(jid);
        else return -1;
    }

    public void openChat(final XmppClient client, String name, final String contactJid, String nick) {
        this.client = client;
        getActivity().setTitle(name);
        this.contactJid = contactJid;
        final RosterItem contact = client.getItem(new Jid(contactJid));
        final boolean isConference = (contact instanceof BookmarkItem);
        if (contact != null)
            if (isConference) {
                if (contact.lastPresence == Presence.PRESENCE_OFFLINE) {
                    client.stream.joinRoom(contact.getJid(), ((BookmarkItem)contact).nick);
                }
            }
        ArrayList<RosterItem> activeContacts = client.activeContacts;
        for (int ii = 0; ii < activeContacts.size(); ++ii){
            if (activeContacts.get(ii).equals(contact))
                activeContacts.remove(contact);
        }
        activeContacts.add(contact);
        messagesAdapter = new MessagesAdapter(getActivity(),  contact.getMessages());
        final EditText t = (EditText) getActivity().findViewById(R.id.messageBox);
        if (nick != null)
            t.append(String.format("%s: ", nick));
        contactChat = (MyListView) getActivity().findViewById(R.id.contact_head);
        contactChat.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
        contactChat.setStackFromBottom(true);
        contactChat.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView from = (TextView) view.findViewById(R.id.messageFrom);
                String text = t.getText().toString();
                if (text.isEmpty()) {
                    String nick = from.getText().toString();
                    t.append(String.format("%s: ", nick));
                }
            }
        });
        contactChat.setAdapter(messagesAdapter);
        ImageButton sendButton = (ImageButton) getActivity().findViewById(R.id.sendButton);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.post(new Runnable() {
                    @Override
                    public void run() {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(
                                getActivity().getCurrentFocus().getWindowToken(),
                                InputMethodManager.HIDE_NOT_ALWAYS);
                        String text = t.getText().toString();
                        if (!text.isEmpty()) {
                            Message outMessage = new Message(contact.getJid().getBare(), text, null, isConference);
                            client.stream.send(outMessage);
                            String id = outMessage.getAttribute("id");
                            t.setText("");
                            if (!isConference) {
                                Msg message = new Msg();
                                message.setId(id);
                                message.setBody(text);
                                message.setFrom(contact.getJid());
                                message.setSubject(getActivity().getString(R.string.ms_me));
                                message.setTimeStamp(System.currentTimeMillis());
                                message.setOutgoing(true);
                                client.messageReceived(contact.getJid(), message);
                                contactChat.invalidateViews();
                            }
                        }
                    }
                });
            }
        });

    }

    @Override
    public void onPause() {
        super.onPause();
        if (!contactChat.isScroll()) addLastPosition(contactJid, contactChat.getFirstVisiblePosition());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (client == null) return;
        client.removeHighlight(contactJid);
        int unreadMessages = client.getMessagesCount(contactJid);
        int lastPosition = getLastPosition(contactJid) + 1;
        if (lastPosition >= 0) {
            contactChat.setScroll(false);
            contactChat.setSelection(lastPosition);
        } else {
            if (unreadMessages > 0) {
                contactChat.setScroll(false);
                contactChat.setSelection(contactChat.getCount() - (unreadMessages + 1));
            } else {
                if (contactChat.isScroll()) contactChat.setSelection(contactChat.getCount());
            }
        }
        client.removeMessagesCount(contactJid);
    }

    public void onScrollStateChanged(AbsListView view, int scrollState) { }
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (firstVisibleItem + visibleItemCount == totalItemCount) contactChat.setScroll(true);
        else contactChat.setScroll(false);
    }

    public void messageReceived(Msg message) {
        //item.image = R.drawable.inc_msg;
        client.removeHighlight(contactJid);
        client.removeMessagesCount(contactJid);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                contactChat.requestLayout();
                if (messagesAdapter != null)
                    messagesAdapter.notifyDataSetChanged();
            }
        });
    }

    public void messageDelivered(final RosterItem contact, final String id) {

        messageReceived(null);
    }
}
