package net.jabjab.views;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import net.jabjab.R;

/**
 * Created with IntelliJ IDEA.
 * User: Vitaly
 * Date: 03.07.12
 * Time: 21:28
 * To change this template use File | Settings | File Templates.
 */
public class JoinRoomView extends DialogFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(R.string.ms_join_conference);
        View v = inflater.inflate(R.layout.joinroom_view, container, false);
        Button btn = (Button) v.findViewById(R.id.btn_join);
        final EditText et_rj = (EditText) v.findViewById(R.id.et_rj);
        final EditText et_nick = (EditText) v.findViewById(R.id.et_nick);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JoinRoomViewListener activity = (JoinRoomViewListener)getActivity();
                String roomJid = et_rj.getText().toString();
                String nick = et_nick.getText().toString();
                activity.onFinishJoinRoomView(roomJid, nick);
                dismiss();
            }
        });
        return v;
    }

    public interface JoinRoomViewListener {
        void onFinishJoinRoomView(String roomJid, String nick);
    }
}