package net.jabjab.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import net.jabjab.R;
import net.jabjab.activities.ChatActivity;
import net.jabjab.client.RosterItem;
import net.jabjab.client.XmppClient;
import net.jabjab.client.muc.MucContact;
import net.jabjab.models.MucUsersAdapter;
import org.bombusmod.xmpp.Jid;

/**
 * Created with IntelliJ IDEA.
 * User: Gerc
 * Date: 26.01.13
 * Time: 20:24
 * To change this template use File | Settings | File Templates.
 */
public class MucUsersView extends Fragment {

    MucUsersAdapter mucUsersAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.muc_users_view, container, false);
    }

    public void viewMucUsers(XmppClient client, final Jid mucJid) {
        final RosterItem c = client.getItem(mucJid);
        getActivity().setTitle(c.getName());
        mucUsersAdapter = new MucUsersAdapter(getActivity(), c.getSubcontacts());
        ListView lv = (ListView) getActivity().findViewById(R.id.muc_users_view);
        lv.setAdapter(mucUsersAdapter);
        lv.setOnItemClickListener(new ListView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long arg3) {
                MucContact mc = mucUsersAdapter.getItem(position);
                Intent ii = new Intent(getActivity().getApplicationContext(),
                        ChatActivity.class);
                ii.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                ii.putExtra("jid", c.getJid().toString());
                ii.putExtra("name", c.getName());
                ii.putExtra("muc_nick", mc.getNick());
                startActivity(ii);
            }
        });
    }
}
