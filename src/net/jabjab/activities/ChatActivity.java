package net.jabjab.activities;

import android.app.NotificationManager;
import android.content.*;
import android.os.Bundle;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import net.jabjab.JabJab;
import net.jabjab.R;
import net.jabjab.client.RosterItem;
import net.jabjab.client.XmppClient;
import net.jabjab.client.muc.MucContact;
import net.jabjab.service.XmppService;
import net.jabjab.views.ChatView;
import net.jabjab.views.MucUsersView;
import net.jabjab.views.RosterView;
import org.bombusmod.xmpp.Jid;
import org.bombusmod.xmpp.MessageDispatcher;
import org.bombusmod.xmpp.Msg;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Vitaly
 * Date: 03.06.12
 * Time: 12:04
 * To change this template use File | Settings | File Templates.
 */
public class ChatActivity extends SherlockFragmentActivity implements MessageDispatcher.MessageListener {

    private String contactJid;
    private String contactName;
    private String mucNick;

    protected XmppClient xmppClient;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_fragment);

        Intent i = getIntent();
        contactJid = i.getStringExtra("jid");
        contactName  = i.getStringExtra("name");
        mucNick = i.getStringExtra("muc_nick");

        xmppClient = JabJab.getService().client;
        xmppClient.messageListeners.add(ChatActivity.this);
           /* TODO: fix tablet layout */
        ChatView view = (ChatView) getSupportFragmentManager().findFragmentById(R.id.chat_fragment);
        view.openChat(xmppClient, contactName, contactJid, mucNick);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        RosterItem contact = xmppClient.getItem(new Jid(contactJid));
        if (contact.getSubcontacts().size() > 0) {
            MenuInflater menuInflater = getSupportMenuInflater();
            menuInflater.inflate(R.menu.chat, menu);
            // Calling super after populating the menu is necessary here to ensure that the
            // action bar helpers have a chance to handle this event.
            return super.onCreateOptionsMenu(menu);
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.muc_users:
                MucUsersView viewer = (MucUsersView) getSupportFragmentManager()
                        .findFragmentById(R.id.muc_users_fragment);
                if (viewer == null || !viewer.isInLayout()) {
                    Intent ii = new Intent(getApplicationContext(),
                            MucUsersActivity.class);
                    ii.putExtra("jid", contactJid);
                    ii.putExtra("name", contactName);
                    startActivity(ii);
                } else {
                    if (contactJid != null) {
                        viewer.viewMucUsers(xmppClient, new Jid(contactJid));
                    }
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void messageReceived(Msg message) {
        ChatView view = (ChatView) getSupportFragmentManager().findFragmentById(R.id.chat_fragment);
        if (view != null) {
            view.messageReceived(message);
        }
    }
}
