/*
 * Copyright 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.jabjab.activities;

import android.app.NotificationManager;
import android.content.*;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentManager;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.alsutton.jabber.datablocks.Presence;
import net.jabjab.JabJab;
import net.jabjab.R;
import net.jabjab.client.RosterItem;
import net.jabjab.client.XmppClient;
import net.jabjab.service.XmppService;
import net.jabjab.views.*;
import org.bombusmod.xmpp.*;
import org.bombusmod.xmpp.login.LoginListener;

public class MainActivity extends SherlockFragmentActivity implements LoginListener, RosterListener,
        RosterView.OnItemSelectedListener,
        MessageDispatcher.MessageListener,
        JoinRoomView.JoinRoomViewListener,
        SignInView.SignInViewListener,
        StatusesView.StatusesViewListener {

    protected XmppClient xmppClient;
    private boolean isBindService = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       /* StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .penaltyDialog()
                .build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectAll()
                .penaltyLog()
                .build());*/
        setContentView(R.layout.main);
        setTitle("");
        if (!isBindService) {
            createAndBindService();
        }
    }

    @Override
    public void onPostResume() {
        super.onPostResume();
        final RosterView rv = (RosterView) getSupportFragmentManager().findFragmentById(R.id.roster_fragment);
        if (rv != null) {
            rv.Update();
        }
    }

    private void createAndBindService() {
        isBindService = true;
        Intent connIntent = new Intent(this, XmppService.class);
        startService(connIntent);
        bindService(connIntent, XmppServiceConnection, Context.BIND_AUTO_CREATE);
    }

    private void destroyService() {
        isBindService = false;
        unbindService(XmppServiceConnection);
        Intent connIntent = new Intent(this, XmppService.class);
        stopService(connIntent);
    }

    @Override
    public void onDestroy() {
        unbindService(XmppServiceConnection);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getSupportMenuInflater();
        menuInflater.inflate(R.menu.main, menu);

        // Calling super after populating the menu is necessary here to ensure that the
        // action bar helpers have a chance to handle this event.
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Toast.makeText(this, "Tapped home", Toast.LENGTH_SHORT).show();
                break;

            case R.id.menu_status:
                showStatusesDialog();
                break;

            case R.id.menu_conference:
                showJoinDialog();
                break;

            case R.id.menu_signout:
                xmppClient.logout();
                showSignInDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private ServiceConnection XmppServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            XmppService xmppService = ((XmppService.LocalBinder) iBinder).getService();
            JabJab.setService(xmppService);
            xmppClient = xmppService.client;
            xmppClient.messageListeners.add(MainActivity.this);
            RosterView viewer = (RosterView) getSupportFragmentManager().findFragmentById(R.id.roster_fragment);
            viewer.viewRoster(xmppClient);
            if (!xmppClient.isSignedIn) {
                SharedPreferences prefs = getSharedPreferences(JabJab.CREDS_PREFS, 0);
                if (prefs.getString("jid", "").equals("")) {
                    showSignInDialog();
                } else {
                    login(prefs.getString("jid", ""), prefs.getString("pass", ""));
                }
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            isBindService = false;
        }
    };

    @Override
    public void itemUpdated(final RosterItem c, final Jid rosterItem, final String nick, final int status, final String statusText, final String groups) {
        final RosterView rv = (RosterView) getSupportFragmentManager().findFragmentById(R.id.roster_fragment);
        if (rv == null || !rv.isInLayout())
            return;
        rv.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                rv.itemUpdated(c, rosterItem, nick, status, statusText, groups);
            }
        });
    }

    @Override
    public void itemSelected(String name, Jid jid) {
        ChatView viewer = (ChatView) getSupportFragmentManager()
                .findFragmentById(R.id.chat_fragment);

        if (viewer == null || !viewer.isInLayout()) {
            Intent ii = new Intent(getApplicationContext(),
                    ChatActivity.class);
            ii.putExtra("jid", jid.toString());
            ii.putExtra("name", name);
            startActivity(ii);
        } else {
            if (jid != null) {
                viewer.openChat(xmppClient, name, jid.toString(), null);
            }
        }
    }

    @Override
    public void loginFailed(final String error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setTitle(error);
            }
        });
    }

    @Override
    public void loginSuccess() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final RosterView rv = (RosterView) getSupportFragmentManager().findFragmentById(R.id.roster_fragment);
                if (rv != null) {
                    rv.setProgress(0);
                }
            }
        });
    }

    @Override
    public void loginState(final LoginState loginState) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int loginMessageId = 0;
                switch (loginState) {
                    case LOGIN_STATE_CONNECTING:
                        loginMessageId = R.string.ms_connect_to;
                        break;
                    case LOGIN_STATE_TLS_NEGOTIATION:
                        // todo: move to resources
                        loginMessageId = R.string.ms_tls_negotiation;
                        break;
                    case LOGIN_STATE_ZLIB_NEGOTIATION:
                        loginMessageId = R.string.ms_compression;
                        break;
                    case LOGIN_STATE_AUTH:
                        loginMessageId = R.string.ms_auth;
                        break;
                    case LOGIN_STATE_SESSION_BINDING:
                        loginMessageId = R.string.ms_session;
                        break;
                    case LOGIN_STATE_RESOURCE_BINDING:
                        loginMessageId = R.string.ms_resource_binding;
                        break;
                    case LOGIN_STATE_SUCCESS:
                        loginMessageId = R.string.ms_connected;
                        break;
                    case LOGIN_STATE_FAILED:
                        loginMessageId = R.string.ms_failed;
                        break;
                }
                final RosterView rv = (RosterView) getSupportFragmentManager().findFragmentById(R.id.roster_fragment);
                if (rv != null) {
                    rv.setProgress(loginMessageId);
                }
            }
        });
    }

    @Override
    public void bindResource(String myJid) {

    }

    @Override
    public void loggedOut() {
        final RosterView rv = (RosterView) getSupportFragmentManager().findFragmentById(R.id.roster_fragment);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                rv.setProgress(R.string.ms_offline);
                if (rv != null && rv.isInLayout())
                    rv.logOff();
            }
        });
    }

    public void login(String user, String pass) {
        xmppClient.rosterListener = MainActivity.this;
        xmppClient.loginListener = MainActivity.this;
        Account account = new Account();
        account.JID = new Jid(user);
        account.password = pass;
        xmppClient.login(account);
    }

    public void showStatusesDialog() {
        FragmentManager fm = getSupportFragmentManager();
        StatusesView sv = new StatusesView();
        sv.show(fm, "change-status");
    }

    public void showJoinDialog() {
        FragmentManager fm = getSupportFragmentManager();
        JoinRoomView jv = new JoinRoomView();
        jv.show(fm, "join");
    }

    public void showSignInDialog() {
        FragmentManager fm = getSupportFragmentManager();
        SignInView sv = new SignInView();
        sv.show(fm, "sign-in");
    }

    @Override
    public void messageReceived(Msg message) {
        final RosterView rv = (RosterView) getSupportFragmentManager().findFragmentById(R.id.roster_fragment);
        if (rv != null) {
            rv.Update();
        }
    }

    @Override
    public void onFinishJoinRoomView(final String roomJid, final String nick) {
        final Jid confJid = new Jid(roomJid);
        final RosterView rv = (RosterView) getSupportFragmentManager().findFragmentById(R.id.roster_fragment);
        if (rv != null) {
            if (rv.isInLayout()) {
                rv.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        xmppClient.stream.joinRoom(confJid, nick);
                    }
                });
            }
        }
    }

    @Override
    public void onFinishSignInView(String jid, String pass) {
        login(jid, pass);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        final RosterView rv = (RosterView) getSupportFragmentManager().findFragmentById(R.id.roster_fragment);
        if (rv != null) {
            if (rv.isInLayout()) {
                rv.Update();
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if ("net.jabjab.notify".equals(intent.getAction())) {
            String jid = intent.getStringExtra("jid");
            String name = intent.getStringExtra("name");
            itemSelected(name, new Jid(jid));
            NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            nm.cancel(XmppService.NOTIFY_MESSAGE);
        }
    }

    @Override
    public void onChangeStatus(int newStatus) {
        xmppClient.setStatus(newStatus);
        if (newStatus == Presence.PRESENCE_OFFLINE)
            destroyService();
        else {
            if (!isBindService) {
                createAndBindService();
            }
        }
    }

}
