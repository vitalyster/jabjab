package net.jabjab.activities;

import android.content.Intent;
import android.os.Bundle;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import net.jabjab.JabJab;
import net.jabjab.R;
import net.jabjab.client.XmppClient;
import net.jabjab.views.MucUsersView;
import org.bombusmod.xmpp.Jid;

/**
 * Created with IntelliJ IDEA.
 * User: Gerc
 * Date: 26.01.13
 * Time: 20:10
 * To change this template use File | Settings | File Templates.
 */
public class MucUsersActivity extends SherlockFragmentActivity {

    private Jid contactJid;
    private String contactName;

    protected XmppClient xmppClient;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.muc_users_fragment);
        Intent i = getIntent();
        contactJid = new Jid(i.getStringExtra("jid"));

        MucUsersView view = (MucUsersView) getSupportFragmentManager().findFragmentById(R.id.muc_users_fragment);
        xmppClient = JabJab.getService().client;
        view.viewMucUsers(xmppClient, contactJid);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
