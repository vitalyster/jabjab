package net.jabjab.client;

import com.alsutton.jabber.datablocks.Presence;
import net.jabjab.client.muc.MucContact;
import org.bombusmod.xmpp.Jid;
import org.bombusmod.xmpp.Msg;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Vitaly
 * Date: 16.06.12
 * Time: 11:19
 * To change this template use File | Settings | File Templates.
 */
public class RosterItem {

    private final List<String> groups = new ArrayList<String>();
    private final List<MucContact> subcontacts = new ArrayList<MucContact>();
    private String name = "";

    public int lastPresence = Presence.PRESENCE_OFFLINE;

    private String statusText = "";

    private Jid jid;

    public int image;
    private final List<Msg> messages = new ArrayList<Msg>();

    @Override
    public boolean equals(Object to) {
        return getJid().getBare().equals(((RosterItem)to).getJid().getBare());
    }

    public List<String> getGroups() {
        return groups;
    }

    public void addGroup(String groupName) {
        groups.add(groupName);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public List<Msg> getMessages() {
        return messages;
    }

    public Jid getJid() {
        return jid;
    }

    public void setJid(Jid jid) {
        this.jid = jid;
    }

    public List<MucContact> getSubcontacts() {
        return subcontacts;
    }
}
