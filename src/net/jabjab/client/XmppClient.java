package net.jabjab.client;

import com.alsutton.jabber.JabberDataBlock;
import com.alsutton.jabber.JabberListener;
import com.alsutton.jabber.JabberStream;
import com.alsutton.jabber.datablocks.Presence;
import net.jabjab.client.muc.BookmarkItem;
import net.jabjab.service.NetworkTask;
import org.bombusmod.xmpp.*;
import org.bombusmod.xmpp.extensions.IqVersionReply;
import org.bombusmod.xmpp.login.LoginListener;
import org.bombusmod.xmpp.login.SASLAuth;
import org.bombusmod.xmpp.muc.BookmarkQuery;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Vitaly
 * Date: 16.06.12
 * Time: 12:24
 * To change this template use File | Settings | File Templates.
 */
public class XmppClient implements JabberListener, LoginListener {

    public JabberStream stream;
    public List<RosterItem> roster = new ArrayList<RosterItem>();
    public ArrayList<RosterItem> activeContacts = new ArrayList<RosterItem>();
    public RosterListener rosterListener;
    public LoginListener loginListener;
    public NotifyListener notifyListener;
    public final List<MessageDispatcher.MessageListener> messageListeners = new ArrayList<MessageDispatcher.MessageListener>();
    private static Hashtable<String, Integer> messagesCount = new Hashtable<String, Integer>();
    private static List<String> mucHighlightsList = new ArrayList<String>();
    // todo: List<Account>
    public Account account;
    public boolean isSignedIn = false;
    public String password = "";

    private int status;

    public void login(Account account) {
        this.account = account;
        new NetworkTask().execute(this);
    }

    public void logout() {
        if (stream != null)
            stream.closeConnection();
    }

    @Override
    public void beginConversation() {
        stream.addBlockListener(new SASLAuth(account, this, stream));
    }

    @Override
    public void connectionTerminated(Exception e) {
        new NetworkTask().execute(this); // reconnect
    }

    @Override
    public void dispatcherException(Exception e, JabberDataBlock data) {
        e.printStackTrace();
        isSignedIn = false;
        loginFailed("Error");
    }

    @Override
    public void connectionClosed() {
        isSignedIn = false;
        loggedOut();
    }

    public void loginFailed(String error) {
        isSignedIn = false;
        if (loginListener != null)
            loginListener.loginFailed(error);
    }

    public void loginSuccess() {
        if (loginListener != null)
            loginListener.loginSuccess();
        isSignedIn = true;
        stream.addBlockListener(new MessageDispatcher(this));
        stream.addBlockListener(new PresenceDispatcher(this));
        stream.addBlockListener(new RosterDispatcher(this));
        stream.addBlockListener(new EntityCaps(this));
        stream.addBlockListener(new IqVersionReply(this));
        stream.send(RosterDispatcher.QueryRoster());
        stream.addBlockListener(new BookmarkQuery(this, BookmarkQuery.LOAD));
        stream.send(new Presence(Presence.PRESENCE_ONLINE, 0, null, null));
    }

    @Override
    public void loginState(LoginState loginState) {
        if (loginListener != null) {
            loginListener.loginState(loginState);
        }
    }

    public void bindResource(String myJid) {
        if (loginListener != null)
            loginListener.bindResource(myJid);
    }

    public void loggedOut() {
        isSignedIn = false;
        if (loginListener != null)
            loginListener.loggedOut();
    }

    public void itemUpdated(RosterItem c, Jid rosterItem, String nick, int status, String statusText, String groups) {
        if (rosterListener != null)
            rosterListener.itemUpdated(c, rosterItem, nick, status, statusText, groups);
    }

    public RosterItem getItem(Jid itemJid) {
        for (int i = 0; i < roster.size(); ++i) {
            RosterItem item = roster.get(i);
            if (item.getJid().getBare().equals(itemJid.getBare())) {
                return item;
            }
        }
        return null;
    }

    public void messageReceived(Jid toJid, Msg message) {
        RosterItem contact = getItem(toJid);
        contact.getMessages().add(message);
        addMessagesCount(toJid.toString());
        if (contact instanceof BookmarkItem && !message.getBody().contains(((BookmarkItem) contact).nick)) {
            message.setOutgoing(true);
        }
        boolean notify = !message.isOutgoing();
        if (notify) {
            mucHighlightsList.add(toJid.toString());
            notifyListener.onNotify(contact, message);
        }
        OnMessageReceived(message);
    }

    public void OnMessageReceived(Msg message) {
        for (MessageDispatcher.MessageListener l : messageListeners) {
            l.messageReceived(message);
        }
    }

    public void messageDelivered(Jid toJid, String id) {
        RosterItem contact = getItem(toJid);
        for (Msg msg : contact.getMessages()) {
            if (msg.getId().equals(id)) {
                msg.setDelivered(true);
                OnMessageReceived(msg);
                return;
            }
        }
    }

    public int getMessagesCount() {
        int result = 0;
        for (Integer val: messagesCount.values()) {
            result += val;
        }
        return result;
    }

    public int getMessagesCount(String jid) {
        if (messagesCount.containsKey(jid)) return messagesCount.get(jid);
        else return 0;
    }

    public void addMessagesCount(String jid) {
        if (messagesCount.containsKey(jid)) {
            messagesCount.put(jid, messagesCount.get(jid) + 1);
        } else {
            messagesCount.put(jid, 1);
        }
    }

    public void removeMessagesCount(String currentJid) {
        if (messagesCount.containsKey(currentJid)) messagesCount.remove(currentJid);
    }

    public boolean isHighlight(String jid) {
        return mucHighlightsList.contains(jid);
    }

    public void removeHighlight(String jid) {
        while (mucHighlightsList.contains(jid))
            mucHighlightsList.remove(jid);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
        if (stream != null) {
            stream.send(new Presence(status, 0, null, null));
        }
        if (status == Presence.PRESENCE_OFFLINE) {
            stream.closeConnection();
        } else {
            login(account);
        }
    }

    public interface NotifyListener {
        void onNotify(RosterItem contact, Msg message);
    }
}
