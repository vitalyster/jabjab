package net.jabjab.client.muc;

import com.alsutton.jabber.datablocks.Presence;
import org.bombusmod.xmpp.Jid;


/**
 * Created with IntelliJ IDEA.
 * User: Gerc
 * Date: 19.01.13
 * Time: 20:24
 * To change this template use File | Settings | File Templates.
 */
public class MucContact {

    public Jid realJid;
    public String affiliation;
    public String role;
    private String nickConf;
    private Jid jidConf;
    private int status = Presence.PRESENCE_OFFLINE;
    private String statusText;

    public MucContact(String nick, Jid jid) {
        nickConf = nick;
        jidConf = jid;
    }

    public String getNick() {
        return nickConf;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public Jid getJid() {
        return jidConf;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }
}
