/*
 * BookmarkItem.java
 *
 * Created on 17.09.2005, 23:21
 * Copyright (c) 2005-2008, Eugene Stahov (evgs), http://bombus-im.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * You can also redistribute and/or modify this program under the
 * terms of the Psi License, specified in the accompanied COPYING
 * file, as published by the Psi Project; either dated January 1st,
 * 2005, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
package net.jabjab.client.muc;

import com.alsutton.jabber.JabberDataBlock;
import net.jabjab.client.RosterItem;
import org.bombusmod.xmpp.Jid;

/**
 * @author EvgS
 */
public class BookmarkItem extends RosterItem {

    public String nick;
    public String password;
    public boolean autojoin = false;
    public boolean isUrl;

    public String toString() {
        if (getName().length() > 0) {
            return getName();
        }
        return (nick == null) ? getJid().toString() : getJid().toString() + '/' + nick;
    }

    public String getJidNick() {
        //return jid + '/' + ((nick.length() > 0) ? nick : StaticData.getInstance().account.getNickName());
        return getJid().toString() + '/' + nick;
    }

    public BookmarkItem(JabberDataBlock data) {
        isUrl = !data.getTagName().equals("conference");
        setName(data.getAttribute("name"));
        try {
            String ajoin = data.getAttribute("autojoin").trim();
            autojoin = ajoin.equals("true") || ajoin.equals("1");
        } catch (Exception e) {
        }
        setJid(new Jid(data.getAttribute((isUrl) ? "url" : "jid")));
        nick = data.getChildBlockText("nick");
        password = data.getChildBlockText("password");
    }

    public BookmarkItem(String name, Jid jid, String nick, String password, boolean autojoin) {
        setName(name);
        setJid(jid);
        this.nick = nick;
        this.password = password;
        this.autojoin = autojoin;
    }

    public JabberDataBlock constructBlock() {
        JabberDataBlock data = new JabberDataBlock((isUrl) ? "url" : "conference");
        data.setAttribute("name", (getName().equals("")) ? getJid().toString() : getName());
        data.setAttribute((isUrl) ? "url" : "jid", getJid().toString());
        data.setAttribute("autojoin", (autojoin) ? "true" : "false");
        if (nick != null) {
            if (nick.length() > 0) {
                data.addChild("nick", nick);
            }
        }
        if (password.length() > 0) {
            data.addChild("password", password);
        }

        return data;
    }
}
