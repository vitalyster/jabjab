package net.jabjab.client;

import com.alsutton.jabber.datablocks.Presence;
import org.bombusmod.xmpp.Jid;


/**
 * Created with IntelliJ IDEA.
 * User: Gerc
 * Date: 19.01.13
 * Time: 20:24
 * To change this template use File | Settings | File Templates.
 */
public class StatusItem {

    private int status = Presence.PRESENCE_OFFLINE;
    private String statusText;

    public StatusItem() {

    }
    public StatusItem(int status) {
        setStatus(status);
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }
}
