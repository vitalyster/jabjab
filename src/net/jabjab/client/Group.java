package net.jabjab.client;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Gerc
 * Date: 02.02.13
 * Time: 12:26
 * To change this template use File | Settings | File Templates.
 */
public final class Group {
    private String groupName;
    private List<RosterItem> rosterItems = new ArrayList<RosterItem>();
    private int onlineCount = 0;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<RosterItem> getRosterItems() {
        return rosterItems;
    }

    public void setRosterItems(List<RosterItem> rosterItems) {
        this.rosterItems = rosterItems;
    }

    public int getOnlineCount() {
        return onlineCount;
    }

    public void setOnlineCount(int onlineCount) {
        this.onlineCount = onlineCount;
    }
}
