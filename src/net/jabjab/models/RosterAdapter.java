package net.jabjab.models;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.alsutton.jabber.datablocks.Presence;
import net.jabjab.R;
import net.jabjab.client.Group;
import net.jabjab.client.RosterItem;
import net.jabjab.client.XmppClient;
import net.jabjab.views.RosterView;
import org.bombusmod.xmpp.Jid;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Vitaly
 * Date: 16.06.12
 * Time: 11:23
 * To change this template use File | Settings | File Templates.
 */
public class RosterAdapter extends BaseAdapter {
    private Context baseContext;
    private static XmppClient client;
    private HashMap<String, Integer> groupsIndex = new HashMap<String, Integer>();
    private HashMap<RosterItem, Integer> contactsIndex = new HashMap<RosterItem, Integer>();
    public static final List<Group> groups = new ArrayList<Group>();
    private List<RosterItem> rosterItems;
    private List<RosterItem> contactsWithMessages;
    private int selectedGroup;
    private static final int ALL_CONTACTS = 0;
    private static final int CONTACTS_WITH_MESSAGES = 1;
    private static final int CUSTOM_GROUPS = 2;
    private ArrayAdapter<String> groupsAdapter;

    public int getSelectedGroup() {
        return selectedGroup;
    }

    public void setSelectedGroup(int selectedGroup) {
        this.selectedGroup = selectedGroup;
    }

    public ArrayAdapter<String> getGroupsAdapter() {
        return groupsAdapter;
    }

    public void setGroupsAdapter(ArrayAdapter<String> groupsAdapter) {
        this.groupsAdapter = groupsAdapter;
    }

    public void init(Context context, XmppClient client) {
        baseContext = context;
        this.client = client;
        for (int i = 0; i < groups.size(); ++i) {
            groupsAdapter.add(groups.get(i).getGroupName());
        }
        rosterItems = client.roster;
        contactsWithMessages = client.activeContacts;
    }


    Comparator<RosterItem> statusComparator = new Comparator<RosterItem>() {
        @Override
        public int compare(RosterItem c1, RosterItem c2) {
            Boolean c1Online = c1.lastPresence < Presence.PRESENCE_OFFLINE;
            Boolean c2Online = c2.lastPresence < Presence.PRESENCE_OFFLINE;
            return c2Online.compareTo(c1Online);
        }
    };
    private static Comparator<RosterItem> nameComparator = new Comparator<RosterItem>() {
        @Override
        public int compare(RosterItem c1, RosterItem c2) {
            String s1 = c1.getName();
            String s2 = c2.getName();
            return s1.toLowerCase().compareTo(s2.toLowerCase());
        }
    };

    private void sortContacts(List<RosterItem> contacts) {
        Collections.sort(contacts, nameComparator);
        Collections.sort(contacts, statusComparator);
    }

    public synchronized int getGroupIndexByName(String groupName) {
        Integer groupId = groupsIndex.get(groupName);
        if (groupId != null)
            return groupId;
        groupsAdapter.add(groupName);
        Group gr = new Group();
        gr.setGroupName(groupName);
        groups.add(gr);
        int pos = groups.size() - 1;
        groupsIndex.put(groupName, pos);
        return pos;
    }

    public void addItem(RosterItem contact) {
        for (String groupName : contact.getGroups()) {
            int groupIndex = getGroupIndexByName(groupName);
            Group group = groups.get(groupIndex);
            group.getRosterItems().add(contact);
            rosterItems.add(contact);
            contactsIndex.put(contact, groupIndex);
        }
    }

    public void addItem(String groupName, RosterItem contact) {
        int groupIndex = getGroupIndexByName(groupName);
        Group group = groups.get(groupIndex);
        group.getRosterItems().add(contact);
        rosterItems.add(contact);
        contactsIndex.put(contact, groupIndex);
    }

    public void clearRoster(Group oldGroup) {
        contactsWithMessages.clear();
        groupsAdapter.remove(oldGroup.getGroupName());
        groups.clear();
        oldGroup.getRosterItems().clear();
        groupsIndex.clear();
        rosterItems.clear();
        contactsIndex.clear();
    }

    public RosterItem getItem(Jid jid) {
        for (int ii = 0; ii < groups.size(); ii++) {
            Group group = groups.get(ii);
            for (int i = 0; i < group.getRosterItems().size(); i++) {
                RosterItem item = group.getRosterItems().get(i);
                if (item.getJid().getBare().equals(jid.getBare())) {
                    return item;
                }
            }
        }
        return null;
    }

    /*private List<RosterItem> getItems() {
        final List<RosterItem> result = new ArrayList<RosterItem>();
        for (int ii = 0; ii < groups.size(); ii++) {
            Group group = groups.get(ii);
            for (int i = 0; i < group.getRosterItems().size(); i++) {
                RosterItem item = group.getRosterItems().get(i);
                result.add(item);
            }
        }
        if (getSelectedGroup() == ALL_CONTACTS && !RosterView.showOnlyOnlines) {
            sortContacts(result);
        }
        return result;
    }*/

    private int onlineCount() {
        int count = 0;
        for (int ii = 0; ii < groups.size(); ii++) {
            Group group = groups.get(ii);
            for (int i = 0; i < group.getRosterItems().size(); i++) {
                RosterItem item = group.getRosterItems().get(i);
                if (item.lastPresence < Presence.PRESENCE_OFFLINE)
                    ++count;
            }
        }
        return count;
    }

    public final void onUpdateContactList(RosterItem user, int oldStatus) {
        Integer groupIndex = contactsIndex.get(user);
        Group group;
        boolean oldStatusIsOffline = oldStatus == Presence.PRESENCE_OFFLINE;
        boolean newStatusIsOffline = user.lastPresence == Presence.PRESENCE_OFFLINE;
        if (groupIndex != null) {
            group = groups.get(groupIndex);
            group.getRosterItems().remove(user);
            rosterItems.remove(user);
            contactsIndex.remove(user);
            addItem(user);
            sortContacts(group.getRosterItems());
            sortContacts(rosterItems);
            if (oldStatusIsOffline != newStatusIsOffline) {
                int onlineCount = group.getOnlineCount();
                if (oldStatusIsOffline)
                    group.setOnlineCount(++onlineCount);
                else
                    group.setOnlineCount(--onlineCount);
            }
        }
    }

    public void sortBookmarks(String groupName) {
        Integer groupId = groupsIndex.get(groupName);
        if (groupId != null) {
            Group group = groups.get(groupId);
            sortContacts(group.getRosterItems());
        }
    }

    @Override
    public int getCount() {
        switch (selectedGroup) {
            case ALL_CONTACTS:
                return RosterView.showOnlyOnlines ? rosterItems.size() : onlineCount();
            case CONTACTS_WITH_MESSAGES:
                return contactsWithMessages.size();
            default:
                if (groups.size() == 0) return 0;
                Group gr = groups.get(selectedGroup - CUSTOM_GROUPS);
                return RosterView.showOnlyOnlines ? gr.getOnlineCount() : gr.getRosterItems().size();
        }
    }

    @Override
    public RosterItem getItem(int i) {
        return groups.get(getSelectedGroup() - CUSTOM_GROUPS).getRosterItems().get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convView, ViewGroup viewGroup) {
        ItemWrapper wr;
        if (convView == null) {
            LayoutInflater inf = ((Activity) baseContext).getLayoutInflater();
            convView = inf.inflate(R.layout.roster_item, null);
            wr = new ItemWrapper(convView);
            convView.setTag(wr);
        } else {
            wr = (ItemWrapper) convView.getTag();
        }
        if (getSelectedGroup() == ALL_CONTACTS) {
            wr.populateFrom(rosterItems.get(i));
        } else if (getSelectedGroup() == CONTACTS_WITH_MESSAGES) {
            wr.populateFrom(contactsWithMessages.get(i));
        } else {
            Group g = groups.get(getSelectedGroup() - CUSTOM_GROUPS);
            wr.populateFrom(g.getRosterItems().get(i));
        }
        return convView;
    }

    public static class ItemWrapper {
        View item = null;

        private TextView itemName = null;

        private TextView itemJid = null;

        private TextView itemStatus = null;

        private ImageView itemImage = null;

        public ItemWrapper(View item) {
            this.item = item;
        }

        void populateFrom(RosterItem item) {
            getItemName().setText(item.getName());
            getItemStatus().setText(item.getStatusText());
            getItemJid().setText(item.getJid().toString());
            boolean hasMsg = client.getMessagesCount(item.getJid().toString()) > 0;
            if (hasMsg)
                getItemImage().setImageResource(R.drawable.ic_msg);
            else
                getItemImage().setImageResource(item.image);
            if (client.isHighlight(item.getJid().toString()))
                getItemName().setTextColor(0xFFAA2323);
            else
                getItemName().setTextColor(0xFF000000);
        }

        public TextView getItemName() {
            if (itemName == null) {
                itemName = (TextView) item.findViewById(R.id.contactName);
            }
            return itemName;
        }

        public TextView getItemStatus() {
            if (itemStatus == null) {
                itemStatus = (TextView) item.findViewById(R.id.contactStatus);
            }
            return itemStatus;
        }

        public TextView getItemJid() {
            if (itemJid == null) {
                itemJid = (TextView) item.findViewById(R.id.contactJid);
            }
            return itemJid;
        }

        public ImageView getItemImage() {
            if (itemImage == null) {
                itemImage = (ImageView) item.findViewById(R.id.contactImage);
            }
            return itemImage;
        }
    }


}
