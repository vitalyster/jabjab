package net.jabjab.models;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.alsutton.jabber.datablocks.Presence;
import net.jabjab.R;
import net.jabjab.client.StatusItem;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Gerc
 * Date: 26.01.13
 * Time: 20:26
 * To change this template use File | Settings | File Templates.
 */
public class StatusesAdapter extends BaseAdapter {

    private Context baseContext;
    private List<StatusItem> statuses;

    public StatusesAdapter(Context context, List<StatusItem> objects){
        baseContext = context;
        statuses = objects;
    }

    @Override
    public int getCount() {
        return statuses.size();
    }

    @Override
    public StatusItem getItem(int i) {
        return statuses.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convView, ViewGroup viewGroup) {
        ItemWrapper wr;
        if (convView == null) {
            LayoutInflater inf = ((Activity)baseContext).getLayoutInflater();
            convView = inf.inflate(R.layout.status_item, null);
            wr = new ItemWrapper(convView);
            convView.setTag(wr);
        } else {
            wr = (ItemWrapper)convView.getTag();
        }
        wr.populateFrom(statuses.get(i));
        return convView;
    }

    public class ItemWrapper {
        View item = null;

        private TextView itemStatus = null;

        private ImageView itemImage = null;

        public ItemWrapper(View item) {
            this.item = item;
        }

        void populateFrom(StatusItem item) {
            getItemStatus().setText(item.getStatusText());
            getItemImage().setImageResource(getStatus(item));
        }

        private int getStatus(StatusItem item) {
            int status = 0;
            switch (item.getStatus()) {
                case Presence.PRESENCE_ONLINE:
                    status = R.drawable.ic_item_online;
                    break;
                case Presence.PRESENCE_OFFLINE:
                    status = R.drawable.ic_item;
                    break;
                case Presence.PRESENCE_AWAY:
                    status = R.drawable.ic_item_away;
                    break;
                case Presence.PRESENCE_CHAT:
                    status = R.drawable.ic_item_chat;
                    break;
                case Presence.PRESENCE_DND:
                    status = R.drawable.ic_item_dnd;
                    break;
                case Presence.PRESENCE_XA:
                    status = R.drawable.ic_item_na;
                    break;
                case Presence.PRESENCE_AUTH:
                    status = R.drawable.ic_item_auth;
                    break;
                case Presence.PRESENCE_INVISIBLE:
                    status = R.drawable.ic_item_inv;
                    break;
            }
            return status;
        }

        public TextView getItemStatus() {
            if (itemStatus == null) {
                itemStatus = (TextView) item.findViewById(R.id.itemStatus);
            }
            return itemStatus;
        }

        public ImageView getItemImage() {
            if (itemImage == null) {
                itemImage = (ImageView) item.findViewById(R.id.statusImage);
            }
            return itemImage;
        }
    }
}
