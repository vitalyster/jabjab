package net.jabjab.models;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import net.jabjab.R;
import org.bombusmod.xmpp.Msg;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Vitaly
 * Date: 04.06.12
 * Time: 23:13
 * To change this template use File | Settings | File Templates.
 */
public class MessagesAdapter extends BaseAdapter {
    private int[] colors = new int[]{0x30ffffff, 0x30e8f0f0};

    Context baseContext;
    private List<Msg> items;

    public MessagesAdapter(Context context, List<Msg> objects) {
        baseContext = context;
        items = objects;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Msg getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        MessageWrapper wr;
        Msg msg = getItem(position);
        if (row == null) {
            LayoutInflater inf = ((Activity) baseContext).getLayoutInflater();
            row = inf.inflate(R.layout.chat_item, parent, false);
            wr = new MessageWrapper(row);
            row.setTag(wr);
        } else {
            wr = (MessageWrapper) row.getTag();
        }
        wr.populateFrom(msg);
        int colorPos = position % colors.length;
        row.setBackgroundColor(colors[colorPos]);
        TextView fromView = (TextView) row.findViewById(R.id.messageFrom);
        String name = fromView.getText().toString();
        if (name == null)
            fromView.setVisibility(View.INVISIBLE);
        return row;

    }


    public class MessageWrapper {
        View item = null;

        private TextView messageFrom = null;

        private TextView messageText = null;

        private TextView messageInfo = null;

        private ImageView contactImage = null;

        public MessageWrapper(View item) {
            this.item = item;
        }

        void populateFrom(Msg item) {
            getMessageFrom().setText(item.getSubject());
            TextView text = getMessageText();
            text.setText(item.getBody());
            text.setTypeface(item.isOutgoing() ? Typeface.DEFAULT : Typeface.DEFAULT_BOLD);
            TextView info = getMessageInfo();
            info.setText(item.getTime());
            Typeface tf = item.isDelivered() ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT;
            info.setTypeface(tf);
        }

        public TextView getMessageFrom() {
            if (messageFrom == null) {
                messageFrom = (TextView) item.findViewById(R.id.messageFrom);
            }
            return messageFrom;
        }

        public TextView getMessageText() {
            if (messageText == null) {
                messageText = (TextView) item.findViewById(R.id.messageText);
            }
            return messageText;
        }

        public TextView getMessageInfo() {
            if (messageInfo == null) {
                messageInfo = (TextView) item.findViewById(R.id.messageInfo);
            }
            return messageInfo;
        }

        public ImageView getContactImage() {
            if (contactImage == null) {
                contactImage = (ImageView) item.findViewById(R.id.contactImage);
            }
            return contactImage;
        }
    }
}
