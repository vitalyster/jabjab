package net.jabjab.util;

/**
 * Created with IntelliJ IDEA.
 * User: Vitaly
 * Date: 07.09.12
 * Time: 15:01
 * To change this template use File | Settings | File Templates.
 */
public class SystemInfo {
    private static final String deviceInfo() {
        final String manufact = android.os.Build.BRAND;
        final String model = android.os.Build.MODEL;
        final String manufact_ = manufact.trim().toLowerCase();
        final String model_ = model.trim().toLowerCase();
        String result = "";
        int total = manufact_.length();
        if (total > model_.length()) total = model_.length();
        int matches = 0;
        for (int i = 0; i < total; i++) {
            if (manufact_.charAt(i) == model_.charAt(i)) matches++;
        }
        final int percentage = matches * 100 / total;
        if (percentage > 20) {
            result = model;
        } else {
            result = manufact + " " + model;
        }
        return result;
    }

    public static final String getPlatformName() {
        String platformName = "Android";
        String device = android.os.Build.MODEL;
        String firmware = android.os.Build.VERSION.RELEASE;

        if (device != null)
            platformName = deviceInfo() + "/Android " + firmware;

        return platformName;
    }
}
